import at.fhjoanneum.eht.calc.IDataCalculationStrategy;
import at.fhjoanneum.eht.calc.ParallelCalculationStrategyInstantSingleWindow;
import at.fhjoanneum.eht.utils.PerformanceMeasureUtil;
import org.junit.Before;
import org.junit.Test;

import javax.media.jai.PlanarImage;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Kleino on 07.01.2015.
 */
public class DiplomBenchmark {

    private IDataCalculationStrategy strat1 = new ParallelCalculationStrategyInstantSingleWindow();
    private PlanarImage[] imgArray;

    @Before
    public void setUp() throws Exception {

    }

    @Test
    /**
     * Figure 4.1: Feature computation step JIT vs no JIT
     */
    public void testJitVsNonJit() throws Exception {
        int times = 4;
        int width = 100;


        imgArray = new PlanarImage[times];
        for (int i = 0; i < times; i++) {
            imgArray[i] = IQMTestUtils.createRandomImage(width, width, 1);
        }

        int windowSize = 5;
        int stepSize = 1;

        int startValue = windowSize / 2;
        int endValueX = width - windowSize / 2;
        int endValueY = width - windowSize / 2;

        for (int i = 0; i < times; i++) {
            PerformanceMeasureUtil.runStart("instant_singlewindow");
            strat1.calculate(imgArray[i], windowSize, stepSize, startValue, endValueX, endValueY);
            PerformanceMeasureUtil.runStop("instant_singlewindow");
        }
        PerformanceMeasureUtil.printStatistics();
        StringBuilder sb = new StringBuilder();
        for (Double dur : PerformanceMeasureUtil.getDurations("instant_singlewindow")) {
            sb.append(dur.toString());
            sb.append(',');

        }
        System.out.println(sb.toString());
    }

    @Test
    /**
     * Figure 4.4: Serial vs. parallel execution (change to SerialExecutionStrategy for serial)
     */
    public void testExecutionTime() throws Exception {
        int width = 50;
        int inc = 10;
        int numberOfRuns = 5;
        int timesPerWidth = 10;


        int windowSize = 5;
        int stepSize = 5;

        imgArray = new PlanarImage[timesPerWidth];
        for (int i = 0; i < numberOfRuns; i++) {
            printProgress(i, numberOfRuns);

            width += inc * i;
            int startValue = windowSize / 2;
            int endValueX = width - windowSize / 2;
            int endValueY = width - windowSize / 2;


            for (int j = 0; j < timesPerWidth; j++) {
                imgArray[j] = IQMTestUtils.createRandomImage(width, width, 1);
            }
            //String key = String.format("width:%d step:%d window:%d", width,stepSize,windowSize);
            String key = width+"";
            for (int j = 0; j < timesPerWidth; j++) {
                PerformanceMeasureUtil.runStart(key);
                strat1.calculate(imgArray[j], windowSize, stepSize, startValue, endValueX, endValueY);
                PerformanceMeasureUtil.runStop(key);
            }
        }
        //PerformanceMeasureUtil.printStatistics();
        PerformanceMeasureUtil.printDurations();

    }
    @Test
    /**
     * Figure 4.2: Relationship between execution time and window size
     */
    public void testWindowSize() throws Exception {
        int startWidth = 100;
        int inc = 2;
        int numberOfRuns = 20;
        int timesPerWidth = 20;


        int startWindowSize = 5;
        int stepSize =1;

        imgArray = new PlanarImage[timesPerWidth];
        for (int i = 0; i < numberOfRuns; i++) {
            printProgress(i, numberOfRuns);

            int windowSize = startWindowSize + inc * i;

            int width = startWidth + windowSize;

            int startValue = windowSize / 2;
            int endValueX = width - windowSize / 2;
            int endValueY = width - windowSize / 2;


            for (int j = 0; j < timesPerWidth; j++) {
                imgArray[j] = IQMTestUtils.createRandomImage(width, width, 1);
            }
            //String key = String.format("width:%d step:%d window:%d", width,stepSize,windowSize);
            String key = windowSize+"";
            for (int j = 0; j < timesPerWidth; j++) {
                PerformanceMeasureUtil.runStart(key);
                strat1.calculate(imgArray[j], windowSize, stepSize, startValue, endValueX, endValueY);
                PerformanceMeasureUtil.runStop(key);
            }
        }
        PerformanceMeasureUtil.printStatistics();
        PerformanceMeasureUtil.printDurations();

    }
    @Test
    /**
     * Figure 4.3: Relationship between execution time and step size
     */
    public void testStepSize() throws Exception {
        int startWidth = 100;
        int inc = 1;
        int numberOfRuns = 20;
        int timesPerWidth = 20;


        int startWindowSize = 5;
        int startStepSize = 1;

        imgArray = new PlanarImage[timesPerWidth];
        for (int i = 0; i < numberOfRuns; i++) {
            printProgress(i, numberOfRuns);

            int stepSize = startStepSize + inc * i;
            int windowSize = startWindowSize;

            int width = startWidth;

            int startValue = windowSize / 2;
            int endValueX = width - windowSize / 2;
            int endValueY = width - windowSize / 2;


            for (int j = 0; j < timesPerWidth; j++) {
                imgArray[j] = IQMTestUtils.createRandomImage(width, width, 1);
            }
            //String key = String.format("width:%d step:%d window:%d", width,stepSize,windowSize);
            String key = stepSize+"";
            for (int j = 0; j < timesPerWidth; j++) {
                PerformanceMeasureUtil.runStart(key);
                strat1.calculate(imgArray[j], windowSize, stepSize, startValue, endValueX, endValueY);
                PerformanceMeasureUtil.runStop(key);
            }
        }
        PerformanceMeasureUtil.printStatistics();
        PerformanceMeasureUtil.printDurations();

    }
    private void printProgress(int curr, int max) {
        double prog = (double) curr / (double) max;

        int numSymbols = 100;
        int numStars = (int) (numSymbols * prog);
        int numUnderScore = (int) (numSymbols * (1- prog));

        final StringBuilder sb = new StringBuilder(numSymbols+2);
        sb.append("[");


        for(int i = 0; i < numStars; i++) {
            sb.append('*');
        }
        for(int i = 0; i < numUnderScore; i++) {
            sb.append('_');
        }
        sb.append("]");
        System.out.println(sb.toString());
    }
}
