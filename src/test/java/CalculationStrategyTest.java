import at.fhjoanneum.eht.calc.IDataCalculationStrategy;
import at.fhjoanneum.eht.calc.ParallelCalculationStrategyInstantMultiWindow;
import at.fhjoanneum.eht.calc.ParallelCalculationStrategyInstantSingleWindow;
import at.fhjoanneum.eht.calc.SerialCalculationStrategy;
import at.fhjoanneum.eht.utils.PerformanceMeasureUtil;
import org.junit.Before;
import org.junit.Test;

import javax.media.jai.PlanarImage;
import java.io.IOException;

/**
 * Created by Kleino on 15.07.2014.
 */
public class CalculationStrategyTest {
    //private static Logger logger = Logger.getLogger(CalculationStrategyTest.class);

    int width = 100;
    int height = 100;
    //private IDataCalculationStrategy strat3 = new SingleCalculationStrategy();
    //private IDataCalculationStrategy strat1 = new ParallelCalculationStrategyInstantSingleWindow();
    private IDataCalculationStrategy strat1 = new SerialCalculationStrategy();
    private IDataCalculationStrategy strat2 = new ParallelCalculationStrategyInstantMultiWindow();
    private int times = 5;
    private PlanarImage[] imgArray;


    @Before
    public void setUp() throws Exception {
        //BasicConfigurator.configure();


        imgArray = new PlanarImage[times];
        for (int i = 0; i < times; i++) {
            imgArray[i] = IQMTestUtils.createRandomImage(width, height, 1);
        }
        // IQMTestUtils.createRandomImage(500, 400, 1);

    }

    @Test
    public void testSimple() throws Exception {
        int windowSize = 5;
        int stepSize = 5;
        int startValue = windowSize / 2;
        int endValueX = width - windowSize / 2;
        int endValueY = height - windowSize / 2;

        PerformanceMeasureUtil.runStart("instant_singlewindow");
        strat2.calculate(imgArray[1], windowSize, stepSize, startValue, endValueX, endValueY);
        PerformanceMeasureUtil.runStop("instant_singlewindow");
        PerformanceMeasureUtil.printStatistics();
    }

    @Test
    public void testCalc2() throws IOException, Exception {
        int windowSize = 5;
        int stepSize = 3;

        int startValue = windowSize / 2;
        int endValueX = width - windowSize / 2;
        int endValueY = height - windowSize / 2;


        // for (int j = 0; j < times; j++) {
        //}
        //for (int j = 0; j < times; j++) {
        for (int i = 0; i < times; i++) {
            //PerformanceMeasureUtil.runStart("instant_singlewindow");
            strat1.calculate(imgArray[i], windowSize, stepSize, startValue, endValueX, endValueY);
            //PerformanceMeasureUtil.runStop("instant_singlewindow");
        }
        //}

        for (int j = 0; j < times; j++) {
            for (int i = 0; i < times; i++) {
                PerformanceMeasureUtil.runStart("instant_singlewindow");
                strat1.calculate(imgArray[i], windowSize, stepSize, startValue, endValueX, endValueY);
                PerformanceMeasureUtil.runStop("instant_singlewindow");
            }
        }

        PerformanceMeasureUtil.printStatistics();
    }

    @Test
    public void testCalc() throws IOException, Exception {
        int windowSize = 5;
        int stepSize = 2;

        int startValue = windowSize / 2;
        int endValueX = width - windowSize / 2;
        int endValueY = height - windowSize / 2;


        // for (int j = 0; j < times; j++) {
        for (int i = 0; i < times; i++) {
            //PerformanceMeasureUtil.runStart("instant_multiwindow");
            strat2.calculate(imgArray[i], windowSize, stepSize, startValue, endValueX, endValueY);
            //PerformanceMeasureUtil.runStop("instant_multiwindow");
        }
        //}
        //for (int j = 0; j < times; j++) {
        for (int i = 0; i < times; i++) {
            //PerformanceMeasureUtil.runStart("instant_singlewindow");
            strat1.calculate(imgArray[i], windowSize, stepSize, startValue, endValueX, endValueY);
            //PerformanceMeasureUtil.runStop("instant_singlewindow");
        }
        //}
        for (int j = 0; j < times; j++) {
            for (int i = 0; i < times; i++) {
                PerformanceMeasureUtil.runStart("Parallel");
                strat2.calculate(imgArray[i], windowSize, stepSize, startValue, endValueX, endValueY);
                PerformanceMeasureUtil.runStop("Parallel");
            }
        }
        for (int j = 0; j < times; j++) {
            for (int i = 0; i < times; i++) {
                PerformanceMeasureUtil.runStart("Serial");
                strat1.calculate(imgArray[i], windowSize, stepSize, startValue, endValueX, endValueY);
                PerformanceMeasureUtil.runStop("Serial");
            }
        }



        PerformanceMeasureUtil.printStatistics();
        PerformanceMeasureUtil.printDurations();
    }
}
