import at.fhjoanneum.eht.SemiautoSegmentationOp;
import at.fhjoanneum.eht.SemiautoSegmentationOpDescriptor;
import at.fhjoanneum.eht.calc.IDataCalculationStrategy;
import at.fhjoanneum.eht.calc.ParallelCalculationStrategyInstantMultiWindow;
import at.fhjoanneum.eht.calc.ParallelCalculationStrategyInstantSingleWindow;
import at.fhjoanneum.eht.calc.SerialCalculationStrategy;
import at.fhjoanneum.eht.gui.table.ROILayerDTO;
import at.fhjoanneum.eht.params.OP_SEMIAUTO;
import at.fhjoanneum.eht.utils.PerformanceMeasureUtil;
import at.fhjoanneum.eht.utils.TableUtils;
import at.fhjoanneum.eht.utils.Utils;
import at.fhjoanneum.eht.weka.WekaFileUtil;
import at.mug.iqm.api.model.ImageModel;
import at.mug.iqm.api.model.IqmDataBox;
import at.mug.iqm.api.model.TableModel;
import at.mug.iqm.api.operator.IResult;
import at.mug.iqm.api.operator.ParameterBlockIQM;
import at.mug.iqm.api.operator.WorkPackage;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import javax.imageio.ImageIO;
import javax.media.jai.PlanarImage;
import java.awt.*;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Properties;
import java.util.Vector;


/**
 * Created by Kleino on 31.03.2014.
 */
@RunWith(JUnit4.class)
public class PluginFunctionalityTests {

    @Rule
    public TemporaryFolder temp_folder = new TemporaryFolder();
    Logger logger = LogManager.getLogger(PluginFunctionalityTests.class);
    WorkPackage wp;
    WorkPackage wp2;
    SemiautoSegmentationOp iop;
    SemiautoSegmentationOp iop2;
    ParameterBlockIQM pb;
    Properties properties;
    File OUTPUT_FOLDER;// = new File("D:/Programming/Masterarbeit/iqm/test_output");
    boolean write_to_file;
    private List<ROILayerDTO> layerList;

    @Before
    public void setup() throws IOException {
        properties = new Properties();

        properties.load(PluginFunctionalityTests.class.getResourceAsStream("test.properties"));

        write_to_file = Boolean.parseBoolean(properties.getProperty("write_to_file"));

        if (write_to_file) {
            OUTPUT_FOLDER = temp_folder.newFolder();
            logger.info("Write to file set to true, images will be stored at " + OUTPUT_FOLDER.getPath());
        } else {
            logger.info("Write to file set to false, images will not be stored");
        }

        layerList = IQMTestUtils.createTestROILayerList();

        pb = new ParameterBlockIQM(new SemiautoSegmentationOpDescriptor());
        pb.setParameter(OP_SEMIAUTO.WINDOW_SIZE.pbKey(), 5);
        pb.setParameter(OP_SEMIAUTO.STEP.pbKey(), 5);
        pb.setParameter(OP_SEMIAUTO.LAYERS.pbKey(), layerList);

        //pbMulti.getSources().add()
        iop = new SemiautoSegmentationOp();
        //iop.setCalculationStrategy(new ParallelCalculationStrategyInstantSingleWindow());
        iop.setCalculationStrategy(new SerialCalculationStrategy());

        //iop2 = new SemiautoSegmentationOp();
        //iop2.setCalculationStrategy(new ParallelCalculationStrategyInstantMultiWindow());
        wp = new WorkPackage(iop, pb);
        //wp2 = new WorkPackage(iop2, pb);
    }

    @Test
    public void testSimple() throws Exception {
        IQMTestUtils.addNewRandomImage(wp);
        pb.setParameter(OP_SEMIAUTO.WINDOW_SIZE.pbKey(), 5);
        pb.setParameter(OP_SEMIAUTO.STEP.pbKey(), 5);
        IResult res = iop.run(wp);

        saveImages(res);

    }

    @Test
    public void testStoreAndRetrieveModel() throws Exception {
        IQMTestUtils.addNewRandomImage(wp, 100, 100, 3);
        pb.setParameter(OP_SEMIAUTO.WINDOW_SIZE.pbKey(), 5);
        pb.setParameter(OP_SEMIAUTO.STEP.pbKey(), 2);
        IResult res = iop.run(wp);

        File temp = File.createTempFile("temp_model", ".model");
        temp.deleteOnExit();
        iop.saveTrainedModel(temp);

        iop.reset();
        Object test = WekaFileUtil.loadModel(temp);
        pb.setParameter(OP_SEMIAUTO.CLASSIFIER.pbKey(), test);
        IResult res2 = iop.run(wp);
        Assert.assertTrue(iop.externalClassifierLoaded());

        List<Color> colors = iop.getClassColors();
        List<String> classNames = iop.getClassNames();

        //There should be as many colors as roi layers but one more className
        Assert.assertEquals(layerList.size(), colors.size());
        Assert.assertEquals(layerList.size() + 1, classNames.size());

        //Class and color should be similiar, order is important
        for (int i = 0; i < colors.size(); i++) {
            Assert.assertEquals(layerList.get(i).getColor(), colors.get(i));
            Assert.assertEquals(layerList.get(i).getName(), classNames.get(i));
        }


        File arffFolder = temp_folder.newFolder();
        iop.saveArffFiles(arffFolder);
    }

    @Test
    public void testLoadedClassifier() throws Exception {
        //Create model

        IQMTestUtils.addNewRandomImage(wp, 100, 100, 1);
        pb.setParameter(OP_SEMIAUTO.STEP.pbKey(), 10);
        iop.run(wp);

        File model = temp_folder.newFile("my_model.model");
        iop.saveTrainedModel(model);


        Object[] cls = (Object[]) WekaFileUtil.loadModel(model);
        pb.setParameter(OP_SEMIAUTO.CLASSIFIER.pbKey(), cls);

        wp2 = wp.clone();
        iop2 = new SemiautoSegmentationOp();
        iop2.run(wp2);
        Assert.assertTrue(iop2.externalClassifierLoaded());
        File arffFolder = temp_folder.newFolder();
        Assert.assertEquals(SemiautoSegmentationOp.State.DONE, iop2.getCurrentState());
        iop2.saveArffFiles(arffFolder);
        //Files appear only after unit test is finished
        //Assert.assertEquals(2, arffFolder.listFiles().length);

    }

    @Test(expected = IllegalArgumentException.class)
    public void testInvalidLoadedClassifier1() throws Exception {
        Object[] cls = new Object[4];
        pb.setParameter(OP_SEMIAUTO.CLASSIFIER.pbKey(), cls);
        IQMTestUtils.addNewRandomImage(wp, 100, 100, 1);
        pb.setParameter(OP_SEMIAUTO.STEP.pbKey(), 10);
        iop.run(wp);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInvalidLoadedClassifier2() throws Exception {
        String cls = "test";
        pb.setParameter(OP_SEMIAUTO.CLASSIFIER.pbKey(), cls);
        IQMTestUtils.addNewRandomImage(wp, 100, 100, 1);
        pb.setParameter(OP_SEMIAUTO.STEP.pbKey(), 10);
        iop.run(wp);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInvalidLoadedClassifier3() throws Exception {
        IQMTestUtils.addNewRandomImage(wp, 100, 100, 1);
        pb.setParameter(OP_SEMIAUTO.STEP.pbKey(), 10);
        iop.run(wp);

        File model = temp_folder.newFile("my_model.model");
        iop.saveTrainedModel(model);


        Object[] cls = (Object[]) WekaFileUtil.loadModel(model);
        cls[1] = null;
        pb.setParameter(OP_SEMIAUTO.CLASSIFIER.pbKey(), cls);

        wp2 = wp.clone();
        iop2 = new SemiautoSegmentationOp();
        iop2.run(wp2);
        Assert.assertTrue(iop2.externalClassifierLoaded());
        File arffFolder = temp_folder.newFolder();
        Assert.assertEquals(SemiautoSegmentationOp.State.DONE, iop2.getCurrentState());
        iop2.saveArffFiles(arffFolder);
    }

    @Test
    public void testHashing() throws Exception {

        PlanarImage im1 = IQMTestUtils.createSinusImage(100, 100, 3);
        PlanarImage im2 = IQMTestUtils.createSinusImage(100, 100, 3);
        PlanarImage im3 = IQMTestUtils.createRandomImage(100, 100, 3);
        PlanarImage im4 = IQMTestUtils.createRandomImage(100, 100, 3);

        System.out.println("Test");
        PerformanceMeasureUtil.runStart("hash");
        String hash1 = Utils.calcImageHash(im1);
        String hash2 = Utils.calcImageHash(im2);
        Assert.assertEquals(hash1, hash2);

        String hash3 = Utils.calcImageHash(im3);
        String hash4 = Utils.calcImageHash(im4);
        PerformanceMeasureUtil.runStop("hash");
        PerformanceMeasureUtil.printStatistics();
        Assert.assertNotEquals(hash3, hash4);
    }

    @Test
    public void testRemoveColumns() throws Exception {
        int windowSize = 5;
        int stepSize = 5;
        int startValue = windowSize / 2;
        int endValueX = 100 - windowSize / 2;
        int endValueY = 100 - windowSize / 2;
        PlanarImage pi = IQMTestUtils.createRandomImage(100, 100, 1);
        IDataCalculationStrategy strat2 = new ParallelCalculationStrategyInstantMultiWindow();

        TableModel tm = strat2.calculate(pi, windowSize, stepSize, startValue, endValueX, endValueY);

        TableUtils.removeColums(tm, new int[]{0, 1, 2, 3, 4, 5, 6, 21, 22, 23});

        Assert.assertEquals(tm.getColumnCount(), 26);
        Vector firstRow = (Vector) tm.getDataVector().firstElement();
        for (int i = 0; i < firstRow.size() - 1; i++) {
            //System.out.println(firstRow.get(i).toString());
            Assert.assertFalse(firstRow.get(i) instanceof String);
        }
        //Last column is the classattribute which should be a string
        Assert.assertTrue(firstRow.get(firstRow.size() - 1) instanceof String);
    }

    private void saveImageModel(ImageModel im) {
        String modelName = im.getModelName().replaceAll("[\\s\\-\\+\\.\\^\\:\\,]", "");
        if (modelName.equals("")) {
            modelName = im.toString();
        }
        File output = new File(OUTPUT_FOLDER, modelName + ".png");
        try {
            logger.info("Writing file to " + output.getPath());
            ImageIO.write(im.getImage(), "png", output);
        } catch (IOException e) {
            System.out.println("Something went wrong when trying to save the image");
            e.printStackTrace();
        }

    }

    private void saveImages(IResult res) {
        if (write_to_file) {
            if (res != null && res.hasImages()) {
                for (IqmDataBox idb : res.listImageResults()) {
                    saveImageModel(idb.getImageModel());
                }
            }

        }
    }


    private void loadImage() throws IOException {
        System.setProperty("com.sun.media.jai.disableMediaLib", "true");

        RenderedImage renderedImage = javax.imageio.ImageIO.read(PluginFunctionalityTests.class.getResourceAsStream("test_images/img.png"));
        //RenderedImage renderedImage = javax.imageio.ImageIO.read(PluginTest.class.getResourceAsStream("test_images/col_img.png"));
        PlanarImage planarImage = PlanarImage.wrapRenderedImage(renderedImage);

        ImageModel im = new ImageModel(planarImage);
        im.setModelName("img");
        im.setFileName("img.png");
        IqmDataBox iqmDataBox = new IqmDataBox(im);
        Vector<Object> vector = new Vector<Object>();
        vector.add(iqmDataBox);
        wp.updateSources(vector);
    }

}
