import at.fhjoanneum.eht.SemiautoSegmentationOp;
import at.fhjoanneum.eht.SemiautoSegmentationOpDescriptor;
import at.fhjoanneum.eht.gui.table.ROILayerDTO;
import at.fhjoanneum.eht.params.OP_SEMIAUTO;
import at.fhjoanneum.eht.utils.PerformanceMeasureUtil;
import at.mug.iqm.api.Application;
import at.mug.iqm.api.gui.ROILayerManager;
import at.mug.iqm.api.operator.IResult;
import at.mug.iqm.api.operator.OperatorDescriptorFactory;
import at.mug.iqm.api.operator.ParameterBlockIQM;
import at.mug.iqm.api.operator.WorkPackage;
import at.mug.iqm.img.bundle.descriptors.IqmOpThresholdDescriptor;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.IOException;

/**
 * Created by Kleino on 08.07.2014.
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({Application.class, OperatorDescriptorFactory.class, ROILayerManager.class})
@PowerMockIgnore("javax.swing.*")
public class SingleVsMultiThreadTest {
    Logger logger = LogManager.getLogger(PluginFunctionalityTests.class);


    SemiautoSegmentationOp iopMulti;

    WorkPackage wpMulti;
    ParameterBlockIQM pbMulti;

    WorkPackage wpSingle;
    ParameterBlockIQM pbSingle;

    @Before
    public void setup() throws IOException {
        int windowSize = 5;
        int stepSize = 1;

        java.util.List<ROILayerDTO> layerList = IQMTestUtils.createTestROILayerList();

        pbSingle = new ParameterBlockIQM(new SemiautoSegmentationOpDescriptor());
        //pbSingle.setParameter(OP_SEMIAUTO.BINARIZE.pbKey(), 1);
        //pbSingle.setParameter(OP_SEMIAUTO.OFFSET.pbKey(), 0);
        pbSingle.setParameter(OP_SEMIAUTO.WINDOW_SIZE.pbKey(), 5);
        pbSingle.setParameter(OP_SEMIAUTO.STEP.pbKey(), 3);
        pbSingle.setParameter(OP_SEMIAUTO.LAYERS.pbKey(), layerList);

        pbMulti = new ParameterBlockIQM(new SemiautoSegmentationOpDescriptor());
        //pbMulti.setParameter(OP_SEMIAUTO.BINARIZE.pbKey(), 1);
        //pbMulti.setParameter(OP_SEMIAUTO.OFFSET.pbKey(), 0);
        pbMulti.setParameter(OP_SEMIAUTO.WINDOW_SIZE.pbKey(), 5);
        pbMulti.setParameter(OP_SEMIAUTO.STEP.pbKey(), 3);
        pbMulti.setParameter(OP_SEMIAUTO.LAYERS.pbKey(), layerList);
        //pbMulti.setParameter(OP_SEMIAUTO.PACKAGE_SIZE.pbKey(), 41);


        iopMulti = new SemiautoSegmentationOp();

        wpMulti = new WorkPackage(iopMulti, pbMulti);


    }

    //@Test
    public void testClassifier() throws IOException {
        //loadImage();
        IQMTestUtils.addNewRandomImage(wpMulti);
        //IQMTestUtils.addNewSinusImage(wpMulti);

        IqmOpThresholdDescriptor.registerWithJAI();

        int stepStart = 1;
        int stepStop = 12;

        int windowStart = 3;
        int windowStop = 11;
        int errorCounter = 0;
        //PerformanceMeasureUtil.runStart("whole_test");
        for (int i = stepStart; i < stepStop; i++) {
            for (int j = windowStart; j < windowStop; j++) {
                System.out.println("Executing for StepSize: " + i + " WindowSize: " + j);
                pbMulti.setParameter(OP_SEMIAUTO.WINDOW_SIZE.pbKey(), j);
                pbMulti.setParameter(OP_SEMIAUTO.STEP.pbKey(), i);
                IResult res = null;
                try {
                    res = iopMulti.run(wpMulti);
                } catch (Exception e) {

                    errorCounter++;
                    System.out.println("Error for StepSize: " + i + " WindowSize: " + j);
                    e.printStackTrace();
                }
            }
        }
        System.out.println("ErrrCounter: " + errorCounter);
        org.junit.Assert.assertEquals(0, errorCounter);
        //PerformanceMeasureUtil.runStop("whole_test");
        //PerformanceMeasureUtil.printStatistics();
    }

    //@Test
    public void testRunPerformance() throws Exception {
        int times = 20;

        IQMTestUtils.benchMarkOperator(iopMulti, wpMulti, times);
        PerformanceMeasureUtil.printStatistics();
    }
}
