import at.fhjoanneum.eht.SemiautoSegmentationOp;
import at.fhjoanneum.eht.SemiautoSegmentationOpDescriptor;
import at.fhjoanneum.eht.calc.ParallelCalculationStrategyInstantMultiWindow;
import at.fhjoanneum.eht.calc.ParallelCalculationStrategyInstantSingleWindow;
import at.fhjoanneum.eht.gui.table.ROILayerDTO;
import at.fhjoanneum.eht.params.OP_SEMIAUTO;
import at.mug.iqm.api.operator.ParameterBlockIQM;
import at.mug.iqm.api.operator.WorkPackage;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kleino on 22.07.2014.
 */
@RunWith(JUnit4.class)
public class PluginPerformanceTests {
    Logger logger = LogManager.getLogger(PluginPerformanceTests.class);

    WorkPackage wp;
    WorkPackage wp2;
    SemiautoSegmentationOp iop;
    SemiautoSegmentationOp iop2;
    ParameterBlockIQM pb;

    boolean write_to_file;
    private List<ROILayerDTO> layerList;

    @Before
    public void setup() throws IOException {
        layerList = IQMTestUtils.createTestROILayerList();

        pb = new ParameterBlockIQM(new SemiautoSegmentationOpDescriptor());
        //pb.setParameter(OP_SEMIAUTO.BINARIZE.pbKey(), 1);
        pb.setParameter(OP_SEMIAUTO.WINDOW_SIZE.pbKey(), 5);
        pb.setParameter(OP_SEMIAUTO.STEP.pbKey(), 5);
        pb.setParameter(OP_SEMIAUTO.LAYERS.pbKey(), layerList);

        iop = new SemiautoSegmentationOp();
        iop.setCalculationStrategy(new ParallelCalculationStrategyInstantSingleWindow());

        iop2 = new SemiautoSegmentationOp();
        iop2.setCalculationStrategy(new ParallelCalculationStrategyInstantMultiWindow());
        wp = new WorkPackage(iop, pb);
        wp2 = new WorkPackage(iop2, pb);
    }


    //@Test
    public void testConvergence() throws Exception {
        //loadImage();
        //IQMTestUtils.addNewRandomImage(wpMulti);
        IQMTestUtils.addNewSinusImage(wp);

        IQMTestUtils.benchMarkUntilConvergence(iop, wp);
        IQMTestUtils.benchMarkUntilConvergence(iop2, wp);
    }

    @Test
    public void testWindowSize() throws Exception {
        List<WorkPackage> wpList = new ArrayList<>();

        int windowStartSize = 3;
        for (int i = 10; i > 0; i--) {
            ParameterBlockIQM pb = new ParameterBlockIQM(new SemiautoSegmentationOpDescriptor());
            //pb.setParameter(OP_SEMIAUTO.BINARIZE.pbKey(), 1);
            //.setParameter(OP_SEMIAUTO.OFFSET.pbKey(), 0);
            pb.setParameter(OP_SEMIAUTO.WINDOW_SIZE.pbKey(), i + windowStartSize);
            pb.setParameter(OP_SEMIAUTO.STEP.pbKey(), 5);
            pb.setParameter(OP_SEMIAUTO.LAYERS.pbKey(), layerList);
            wpList.add(new WorkPackage(iop, pb));
        }

        IQMTestUtils.benchMarkOperator(iop, wpList, 1);
    }
    //@Test
    public void testWindowSize2() throws Exception {
        List<WorkPackage> wpList = new ArrayList<>();

        int windowStartSize = 3;
        for (int i = 10; i > 0; i--) {
            ParameterBlockIQM pb = new ParameterBlockIQM(new SemiautoSegmentationOpDescriptor());
            pb.setParameter(OP_SEMIAUTO.WINDOW_SIZE.pbKey(), i + windowStartSize);
            pb.setParameter(OP_SEMIAUTO.STEP.pbKey(), 5);
            pb.setParameter(OP_SEMIAUTO.LAYERS.pbKey(), layerList);
            wpList.add(new WorkPackage(iop, pb));
        }


        for (WorkPackage wp : wpList) {

        }
        IQMTestUtils.benchMarkOperator(iop, wpList, 5);
    }

    //@Test
    public void testStepSize() throws Exception {
        List<WorkPackage> wpList = new ArrayList<>();

        int stepSizeStart = 2;
        for (int i = 10; i >= 0; i--) {
            int stepSize = i + stepSizeStart;

            ParameterBlockIQM pb = new ParameterBlockIQM(new SemiautoSegmentationOpDescriptor());
            //pb.setParameter(OP_SEMIAUTO.BINARIZE.pbKey(), 1);
            //pb.setParameter(OP_SEMIAUTO.OFFSET.pbKey(), 0);
            pb.setParameter(OP_SEMIAUTO.WINDOW_SIZE.pbKey(), 5);
            pb.setParameter(OP_SEMIAUTO.STEP.pbKey(), stepSize);
            pb.setParameter(OP_SEMIAUTO.LAYERS.pbKey(), layerList);
            wpList.add(new WorkPackage(iop, pb));
        }

        IQMTestUtils.benchMarkOperator(iop, wpList, 5);
    }


    //@Test
    public void testPackageSize() throws Exception {
        List<WorkPackage> wpList = new ArrayList<>();

        int packageSizeStart = 1;
        int packageSizeEnd = 50;
        for (int i = packageSizeEnd; i >= 0; i--) {
            int packageSize = i + packageSizeStart;

            ParameterBlockIQM pb = new ParameterBlockIQM(new SemiautoSegmentationOpDescriptor());
            //pb.setParameter(OP_SEMIAUTO.BINARIZE.pbKey(), 1);
            //pb.setParameter(OP_SEMIAUTO.OFFSET.pbKey(), 0);
            pb.setParameter(OP_SEMIAUTO.WINDOW_SIZE.pbKey(), 5);
            pb.setParameter(OP_SEMIAUTO.STEP.pbKey(), 3);
            pb.setParameter(OP_SEMIAUTO.LAYERS.pbKey(), layerList);
            pb.setParameter(OP_SEMIAUTO.PACKAGE_SIZE.pbKey(), packageSize);
            wpList.add(new WorkPackage(iop, pb));
        }

        IQMTestUtils.benchMarkOperator(iop, wpList, 5);
    }

    @Test
    public void testPerformance() throws Exception {
        ParameterBlockIQM pb = new ParameterBlockIQM(new SemiautoSegmentationOpDescriptor());
        //pb.setParameter(OP_SEMIAUTO.BINARIZE.pbKey(), 1);
        //pb.setParameter(OP_SEMIAUTO.OFFSET.pbKey(), 0);
        pb.setParameter(OP_SEMIAUTO.WINDOW_SIZE.pbKey(), 5);
        pb.setParameter(OP_SEMIAUTO.STEP.pbKey(), 3);
        pb.setParameter(OP_SEMIAUTO.LAYERS.pbKey(), layerList);
        //pb.setParameter(OP_SEMIAUTO.PACKAGE_SIZE.pbKey(), 41);

        IQMTestUtils.benchMarkOperator(iop, new WorkPackage(iop, pb), 20);
    }
}
