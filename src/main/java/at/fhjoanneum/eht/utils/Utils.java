package at.fhjoanneum.eht.utils;

import at.mug.iqm.api.model.ImageModel;
import at.mug.iqm.api.model.IqmDataBox;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;

import javax.media.jai.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.awt.image.renderable.ParameterBlock;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

/**
 * Created by Kleino on 15.04.2014.
 */
public class Utils {
    public static PlanarImage binarize(PlanarImage pi) {
        ParameterBlock pb = new ParameterBlock();
        pb.addSource(pi);
        pb.add(1.0); //Threshold

        return JAI.create("binarize", pb);
    }

    public static PlanarImage cropImage(PlanarImage pi, Rectangle bounds) {
        int offSetX = bounds.x;
        int offSetY = bounds.y;
        int newWidth = bounds.width;
        int newHeight = bounds.height;

        ParameterBlock pbCrop = new ParameterBlock();
        pbCrop.addSource(pi);

        pbCrop.add((float) offSetX);
        pbCrop.add((float) offSetY);
        pbCrop.add((float) newWidth);
        pbCrop.add((float) newHeight);
        PlanarImage cropped = JAI.create("Crop", pbCrop, null);
        //System.out.println("---MinX " + cropped.getMinX() + " MaxX " + cropped.getMaxX());
        //System.out.println("MinY " + cropped.getMinY() + " MaxY " + cropped.getMaxY());
        //System.out.println("Width: " + cropped.getWidth() +" Height " + cropped.getHeight());
        //cropped.getMinY();
        ////cropped.getHeight();


        String newName = pi.getProperty("image_name") + "_cropped";
        cropped.setProperty("image_name", newName);
        cropped.setProperty("model_name", newName);


        return cropped;
    }

    public static PlanarImage cropImage(PlanarImage pi, ROIShape rs) {
        TiledImage ti = new TiledImage(pi.getMinX(), pi.getMinY(),
                pi.getWidth(), pi.getHeight(), pi.getTileGridXOffset(),
                pi.getTileGridYOffset(), pi.getSampleModel(),
                pi.getColorModel());
        ti.setData(pi.copyData(), rs);

        String newName = pi.getProperty("image_name") + "_cropped";
        ti.setProperty("image_name", newName);
        ti.setProperty("model_name", newName);
        return ti;
    }

    public static ImageModel generateNewImageModel(PlanarImage pi, String imgName, String fileName) {
        ImageModel im = new ImageModel(pi);
        im.setModelName(imgName);
        im.setFileName(fileName);
        return im;
    }

    public static ImageModel generateNewImageModel(PlanarImage pi, String imgName) {
        return generateNewImageModel(pi, imgName, imgName + ".png");
    }

    /**
     * Overlays the first image with the second image. It is assumed that both images have the same dimensions and
     * that the second image is semi-transparent
     *
     * @param baseImage
     * @param overlay
     * @return
     */
    public static PlanarImage createOverlayImage(PlanarImage baseImage, PlanarImage overlay) {

        int w = Math.max(baseImage.getWidth(), overlay.getWidth());
        int h = Math.max(overlay.getHeight(), overlay.getHeight());

        BufferedImage base = baseImage.getAsBufferedImage();
        BufferedImage overl = makeImageTranslucent(overlay.getAsBufferedImage(), 0.25);


        //int w = Math.max(base.getWidth(), overlay.getWidth());
        //int h = Math.max(base.getHeight(), overlay.getHeight());

        BufferedImage combined = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);

        Graphics g = combined.getGraphics();
        g.drawImage(base, 0, 0, null);
        g.drawImage(overl, 0, 0, null);
        g.dispose();
        BufferedImage img = new BufferedImage(combined.getWidth(), combined.getHeight(), BufferedImage.TYPE_3BYTE_BGR);
        img.setData(combined.getRaster().createChild(0, 0, combined.getWidth(), combined.getHeight(), combined.getMinX(), combined.getMinY(), new int[]{0, 1, 2}));

        PlanarImage resImg = PlanarImage.wrapRenderedImage(img);
        resImg.setProperty("model_name", "overlaid");
        return resImg;
    }

    /**
     * Taken from http://www.java2s.com/Code/Java/2D-Graphics-GUI/MakeimageTransparency.htm
     *
     * @param source
     * @param alpha
     * @return
     */
    public static BufferedImage makeImageTranslucent(BufferedImage source, double alpha) {
        BufferedImage target = new BufferedImage(source.getWidth(),
                source.getHeight(), java.awt.Transparency.TRANSLUCENT);
        // Get the images graphics
        Graphics2D g = target.createGraphics();
        // Set the Graphics composite to Alpha
        g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER,
                (float) alpha));
        // Draw the image into the prepared reciver image
        g.drawImage(source, null, 0, 0);
        // let go of all system resources in this Graphics
        g.dispose();
        // Return the image
        return target;
    }

    public static PlanarImage addBorder(PlanarImage pi, int borderSize) {
        ParameterBlock pb = new ParameterBlock();
        pb.addSource(pi);
        int padding = borderSize;
        pb.add(padding);
        pb.add(padding);
        pb.add(padding);
        pb.add(padding);

        pb.add(BorderExtender.createInstance(BorderExtender.BORDER_REFLECT));

        return JAI.create("border", pb);
    }

    public static void printDescriptiveStatistics(HashMap<String, Vector<Double>> statResults) {
        for (Map.Entry<String, Vector<Double>> entry : statResults.entrySet()) {

            DescriptiveStatistics stats = new DescriptiveStatistics();
            Vector<Double> values = entry.getValue();
            for (Double d : values) {
                if (!d.isNaN()) {
                    stats.addValue(d);
                }
            }
            System.out.println("--Descriptive statistics for: " + entry.getKey() + ", executed " + stats.getN() + " times.");
            System.out.println("Mean: " + stats.getMean());
            System.out.println("Median: " + stats.getPercentile(50));
            System.out.println("Stdev: " + stats.getStandardDeviation());
            System.out.println("Max: " + stats.getMax());
            System.out.println("Min: " + stats.getMin());
            System.out.println("Sum: " + stats.getSum());
        }
    }

    public static IqmDataBox createIqmDataBox(PlanarImage planarImage, String name) {
        ImageModel im = new ImageModel(planarImage);
        im.setModelName(name);
        return new IqmDataBox(im);
    }

    public static PlanarImage resizeImage(float scale, PlanarImage image) {
        ParameterBlock paramBlock = new ParameterBlock();
        paramBlock.addSource(image);
        paramBlock.add(scale);
        paramBlock.add(scale);
        paramBlock.add(0.0f);
        paramBlock.add(0.0f);
        RenderingHints qualityHints = new RenderingHints(RenderingHints.KEY_RENDERING,
                RenderingHints.VALUE_RENDER_QUALITY);
        return JAI.create("Scale", paramBlock, qualityHints);
    }

    public static String calcImageHash(PlanarImage randomImage) {
        String digest = null;
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            DataBufferByte data = (DataBufferByte) randomImage.getAsBufferedImage().getRaster().getDataBuffer();
            byte[] hash = md.digest(data.getData());
            StringBuilder sb = new StringBuilder(2 * hash.length);
            for (byte b : hash) {
                sb.append(String.format("%02x", b & 0xff));
            }
            digest = sb.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return digest;
    }
}
