package at.fhjoanneum.eht.utils;

/*
 * #%L
 * Project: IQM - API
 * File: PerformanceMeasureUtil.java
 * 
 * $Id$
 * $HeadURL$
 * 
 * This file is part of IQM, hereinafter referred to as "this program".
 * %%
 * Copyright (C) 2009 - 2014 Helmut Ahammer, Philipp Kainz
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by Kleino on 23.06.2014.
 */
public class PerformanceMeasureUtil {
    private static ArrayList<Double> currentDurations = new ArrayList<>();
    private static HashMap<Object, ArrayList<Double>> allDurations = new HashMap<>();
    private static HashMap<String, Double> allStarts = new HashMap<>();

    private static double currentStart;

    public static void runStart(String run) {
        allStarts.put(run, (double) System.currentTimeMillis());
    }

    public static void runStop(String run) {
        if (!allDurations.containsKey(run)) {
            allDurations.put(run, new ArrayList<Double>());
        }
        allDurations.get(run).add(System.currentTimeMillis() - allStarts.get(run));
    }

    public static void printStatistics() {
        for (Map.Entry<Object, ArrayList<Double>> entry : allDurations.entrySet()) {
            DescriptiveStatistics stats = new DescriptiveStatistics();
            ArrayList<Double> dur = entry.getValue();
            for (Double d : dur) {
                stats.addValue(d);
            }
            System.out.println("--Exectution summary for " + entry.getKey() + ", executed " + dur.size() + " times.");
            System.out.println("Mean: " + stats.getMean());
            System.out.println("Lower Quartile: " + stats.getPercentile(25));
            System.out.println("Median: " + stats.getPercentile(50));
            System.out.println("Upper Quartile: " + stats.getPercentile(75));
            System.out.println("Stdev: " + stats.getStandardDeviation());
            System.out.println("Max: " + stats.getMax());
            System.out.println("Min: " + stats.getMin());
            System.out.println("Sum execution time: " + stats.getSum());

        }

    }
    public static void printDurations() {
        //For sorting
        Map<Object, ArrayList<Double>> map = new TreeMap<>(allDurations);
        for (Map.Entry<Object, ArrayList<Double>> entry : map.entrySet()) {
            ArrayList<Double> dur = entry.getValue();
            StringBuilder sb = new StringBuilder();
            sb.append(entry.getKey());
            sb.append(',');
            for (Double d : dur) {
                sb.append(d.toString());
                sb.append(',');
            }
            sb = sb.deleteCharAt(sb.length()-1);
//            System.out.println(dur.toString());
            System.out.println(sb.toString());

        }

    }

    public static ArrayList<Double> getDurations(String key) {
        if (!allDurations.containsKey(key)) {
            return new ArrayList<>();
        } else {
            return allDurations.get(key);
        }
    }
}
