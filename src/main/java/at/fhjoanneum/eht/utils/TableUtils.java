package at.fhjoanneum.eht.utils;

import at.mug.iqm.api.model.TableModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

/**
 * Created by Kleino on 09.07.2014.
 */
public class TableUtils {
    public static double getVal(Vector v, int i) {
        double val = 0;
        Object obj = v.get(i);
        if (obj instanceof Double) {
            val = (double) obj;
        } else if (obj instanceof Integer) {
            val = (int) obj;
        } else if (obj instanceof Float) {
            val = (float) obj;
        }
        return val;
    }

    public static void removeColum(TableModel tableModel, int colIndex) {
        for (Object row : tableModel.getDataVector()) {
            ((Vector) row).remove(colIndex);
        }
        ArrayList<String> colums = getModelColumns(tableModel);
        colums.remove(colIndex);
        //tableModel.setColumnIdentifiers(c);
    }

    public static void removeColums(TableModel tableModel, int[] colIndices) {
        //Clone the array
        int[] colIndicesClone = colIndices.clone();
        //Reverse the array
        for (int i = 0; i < colIndicesClone.length / 2; i++) {
            int temp = colIndicesClone[i];
            colIndicesClone[i] = colIndicesClone[colIndicesClone.length - 1 - i];
            colIndicesClone[colIndicesClone.length - 1 - i] = temp;
        }
        //Remove column data
        for (Object row : tableModel.getDataVector()) {
            for (int i = 0; i < colIndicesClone.length; i++) {
                ((Vector) row).remove(colIndicesClone[i]);
            }
        }

        //Remove colum header as well
        ArrayList<String> colums = getModelColumns(tableModel);
        for (int i = 0; i < colIndicesClone.length; i++) {
            colums.remove(colIndicesClone[i]);
        }
        tableModel.setColumnIdentifiers(new Vector(colums));
    }

    public static void add(Vector<Double> sums, Vector row) {
        for (int i = 0; i < sums.size(); i++) {
            double d1 = getVal(sums, i);
            double d2 = getVal(row, i);
            sums.remove(i);
            sums.add(i, d1 + d2);
        }

    }

    public static ArrayList<String> getModelColumns(TableModel tableModel) {
        int colCount = tableModel.getColumnCount();
        ArrayList<String> colNames = new ArrayList<>(colCount);
        for (int i = 0; i < colCount; i++) {

            colNames.add(tableModel.getColumnName(i));
        }
        return colNames;
    }

    private void calcNaNs(TableModel tm) {
        Vector<Vector> unlabled = tm.getDataVector();
        ArrayList<String> columns = TableUtils.getModelColumns(tm);
        HashMap<String, Vector<Double>> statResults = new HashMap<>();
        HashMap<String, Integer> NaNs = new HashMap<>();
        for (String column : columns) {
            if (!String.class.equals(unlabled.get(0).get(columns.indexOf(column)).getClass())) {
                Vector<Double> values = new Vector<>();
                for (Vector row : unlabled) {
                    Double d = TableUtils.getVal(row, columns.indexOf(column));
                    if (d.isNaN()) {
                        if (!NaNs.containsKey(column)) {
                            NaNs.put(column, 1);
                        } else {
                            NaNs.put(column, NaNs.get(column) + 1);
                        }
                        //d = 0.0d;
                    }

                    values.add(d);
                }
                statResults.put(column, values);
            }
        }
        for (Map.Entry<String, Integer> entry : NaNs.entrySet()) {
            System.out.println("-NaNs for " + entry.getKey() + ": " + entry.getValue());
        }
    }
}
