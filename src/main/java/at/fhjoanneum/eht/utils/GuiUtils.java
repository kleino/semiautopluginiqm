package at.fhjoanneum.eht.utils;

import at.fhjoanneum.eht.I18N;
import at.mug.iqm.api.Resources;
import at.mug.iqm.commons.util.PropertyManager;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.File;

/**
 * Created by Kleino on 09.11.2014.
 */
public class GuiUtils {
    public static File showDirectoryDialog() {
        JFileChooser fc = new JFileChooser();

        fc.setDialogTitle(I18N.getMessage("gui.dirchooser.title"));
        fc.setName("CommonTools.SingleFileChooserDialog");

        PropertyManager pm = PropertyManager.getManager(fc.getName());

        String curDir = pm.getProperty("lastDir");
        if (curDir == null) {
            curDir = System.getProperty("user.home");
        }
        fc.setCurrentDirectory(new File(curDir));

        JFrame frame = new JFrame();
        frame.setIconImage(new ImageIcon(Resources
                .getImageURL("icon.application.grey.32x32")).getImage());

        fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        fc.setMultiSelectionEnabled(false);

        int selection = -1;
        selection = fc.showSaveDialog(frame);
        if (selection != JFileChooser.APPROVE_OPTION) {
            return null;
        } else {
            pm.setProperty("lastDir", fc.getCurrentDirectory().toString());
            return fc.getSelectedFile();
        }
    }

    public static File showFileDialog(int dialogType) {

        final String WEKA_MODEL_SUFFIX = "model";
        FileFilter filter = new FileNameExtensionFilter("Weka model", WEKA_MODEL_SUFFIX, "cls");

        JFileChooser fc = new JFileChooser();
        fc.setFileFilter(filter);

        if (dialogType == JFileChooser.SAVE_DIALOG) {
            fc.setDialogTitle(I18N.getMessage("gui.modelchooser.save.title"));
        } else {
            fc.setDialogTitle(I18N.getMessage("gui.modelchooser.retrieve.title"));
        }
        fc.setName("CommonTools.SingleFileChooserDialog");

        PropertyManager pm = PropertyManager.getManager(fc.getName());

        String curDir = pm.getProperty("lastDir");
        if (curDir == null) {
            curDir = System.getProperty("user.home");
        }
        fc.setCurrentDirectory(new File(curDir));

        JFrame frame = new JFrame();
        frame.setIconImage(new ImageIcon(Resources
                .getImageURL("icon.application.grey.32x32")).getImage());

        fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
        fc.setMultiSelectionEnabled(false);


        int selection = -1;
        if (dialogType == JFileChooser.SAVE_DIALOG) {
            selection = fc.showSaveDialog(frame);
        } else if (dialogType == JFileChooser.OPEN_DIALOG) {
            selection = fc.showOpenDialog(frame);
        } else {
            return null;
        }

        if (selection != JFileChooser.APPROVE_OPTION) {
            return null;
        } else {
            pm.setProperty("lastDir", fc.getCurrentDirectory().toString());

            File fileToBeSaved = fc.getSelectedFile();

            if (!fc.getSelectedFile().getAbsolutePath().endsWith(WEKA_MODEL_SUFFIX)) {
                fileToBeSaved = new File(fc.getSelectedFile() + "." + WEKA_MODEL_SUFFIX);
            }
            return fileToBeSaved;
        }
    }
}
