package at.fhjoanneum.eht;

import at.fhjoanneum.eht.gui.table.ROILayerDTO;
import at.fhjoanneum.eht.params.OP_SEMIAUTO;
import at.mug.iqm.api.operator.DefaultImageOperatorValidator;
import at.mug.iqm.api.operator.IWorkPackage;
import at.mug.iqm.api.operator.ParameterBlockIQM;
import weka.classifiers.Classifier;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * This class implements custom validation activities for the operator. One is
 * free to inherit directly from the {@link DefaultImageOperatorValidator} in
 * order to spare double code.
 * <p/>
 * If you want to extend the default validation routine or override it (e.g.
 * perform NO validation at all), simply override
 * {@link DefaultImageOperatorValidator#validate(at.mug.iqm.api.operator.IWorkPackage)}
 * in this class.
 *
 * @author Philipp Kainz
 */
public class SemiautoSegmentationOpValidator extends DefaultImageOperatorValidator {

    public SemiautoSegmentationOpValidator() {
        //TODO: Validation for array
    }

    @Override
    public boolean validateSources(IWorkPackage wp) throws IllegalArgumentException {
        ParameterBlockIQM pb = wp.getParameters();
        Classifier cls = (Classifier) pb.getObjectParameter(OP_SEMIAUTO.CLASSIFIER.pbKey());
        if (cls == null) {
            List<ROILayerDTO> layers = (List<ROILayerDTO>) pb.getObjectParameter(OP_SEMIAUTO.LAYERS.pbKey());
            if (layers != null) {
                if (layers.size() < 2) {
                    throw new IllegalArgumentException("At least two ROI Layers for classifier training needed! Add layers or load classifier");
                } else {
                    for (ROILayerDTO layer : layers) {
                        if (layer.getRoiShapes().size() == 0) {
                            throw new IllegalArgumentException("At least one selected ROI Layer has no shapes defined!");
                        }
                    }
                }
                return true;
            }
            return true;

        } else {
            return true;
        }
    }

    public boolean validateExecution(IWorkPackage wp) {
        ParameterBlockIQM pb = wp.getParameters();
        Object cls = pb.getObjectParameter(OP_SEMIAUTO.CLASSIFIER.pbKey());
        List<ROILayerDTO> layers = (List<ROILayerDTO>) pb.getObjectParameter(OP_SEMIAUTO.LAYERS.pbKey());
        if (cls == null && layers == null) {
            throw new IllegalArgumentException("No Classifier or ROI Layers defined");
        } else if (cls == null && layers != null) {
            if (layers.size() < 2) {
                throw new IllegalArgumentException("At least two ROI Layers for classifier training needed! Add layers or load classifier");
            } else {
                Set<String> duplicateNames = new HashSet<>();

                for (ROILayerDTO layer : layers) {
                    //set.add() returns false, if an entry already exists
                    if (!duplicateNames.add(layer.getName())) {
                        throw new IllegalArgumentException("At least two ROI Layers have the same name!");
                    }
                    if (layer.getRoiShapes().size() == 0) {
                        throw new IllegalArgumentException("At least one selected ROI Layer has no shapes defined!");
                    }
                }
            }

            return true;
        } else if (cls != null) {
            if (cls instanceof Object[]) {
                Object[] suppliedCLs = (Object[]) cls;
                boolean error = false;
                if (suppliedCLs[0] == null || suppliedCLs[1] == null) {
                    error = true;
                }
                if (suppliedCLs.length > 3 || suppliedCLs.length == 0) {
                    error = true;
                }

                if (error) {
                    throw new IllegalArgumentException("Unknown classifier format");
                }

            } else if (!(cls instanceof Classifier)) {
                throw new IllegalArgumentException("Unknown classifier format");
            }


        }
        return true;
    }
}

