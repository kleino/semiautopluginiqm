package at.fhjoanneum.eht.gui;

import at.fhjoanneum.eht.I18N;
import at.fhjoanneum.eht.SemiautoSegmentationOp;
import at.fhjoanneum.eht.SemiautoSegmentationOpDescriptor;
import at.fhjoanneum.eht.gui.popup.ImgTableModel;
import at.fhjoanneum.eht.gui.popup.OptionsPopup;
import at.fhjoanneum.eht.gui.table.*;
import at.fhjoanneum.eht.params.OP_SEMIAUTO;
import at.fhjoanneum.eht.utils.GuiUtils;
import at.fhjoanneum.eht.weka.WekaClassifier;
import at.fhjoanneum.eht.weka.WekaFileUtil;
import at.mug.iqm.api.Application;
import at.mug.iqm.api.gui.DefaultDrawingLayer;
import at.mug.iqm.api.gui.IDrawingLayer;
import at.mug.iqm.api.gui.ROILayerManager;
import at.mug.iqm.api.operator.*;
import at.mug.iqm.commons.util.DialogUtil;

import javax.media.jai.ROIShape;
import javax.swing.*;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableRowSorter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * This class represents the graphical UI of an operator. One may either extend
 * the {@link AbstractImageOperatorGUI} or the {@link AbstractPlotOperatorGUI}
 * in order to create a GUI.
 *
 * @author Philipp Kainz
 */
public class SemiautoSegmentationOpGUI extends AbstractImageOperatorGUI implements
        ChangeListener, TableModelListener {

    /**
     * The UID for serialization.
     */
    private static final long serialVersionUID = -6684951054122832268L;

    /**
     * A private parameter block, holding the current settings for the operator.
     */
    private ParameterBlockIQM pb;


    private JSpinner spWindowSize;
    private JSpinner spStepSize;

    private JLabel lblStateText = null;
    private JLabel lblCurrentState = null;
    private JLabel lblClsStateText;
    private JLabel lblClsCurrentState;

    private JButton buttSaveARFF = null;
    private JButton buttSaveModel = null;
    private JButton buttLoadModel = null;

    private JTable roiTable = null;
    private ROITableModel roiTableModel;

    private JScrollPane roiListScrollPane = null;

    private Thread updateThread = null;
    private JPanel statePanel;
    private OptionsPopup popup;
    private TableRowSorter<ROITableModel> tableRowSorter;


    /**
     * Mandatory empty constructor for usage with the {@link OperatorGUIFactory}
     * .
     */
    public SemiautoSegmentationOpGUI() {
        // within this method all elements for this GUI must be initialized
        logger.debug("Now initializing... operator");

        this.setOpName(new SemiautoSegmentationOpDescriptor().getName());


        this.initialize();

        this.setTitle(I18N.getMessage("gui.title"));
        this.setResizable(false);

        this.getOpGUIContent().setLayout(new GridBagLayout());

        JPanel mainPanel = createMainPanel();
        GridBagConstraints gbc_opgui = new GridBagConstraints();
        gbc_opgui.fill = GridBagConstraints.BOTH;

        JPanel panWindowSize = createWindowSizePanel();
        JPanel panStepSize = createStepSizePanel();

        JPanel panOptions = new JPanel();
        panOptions.add(panWindowSize);
        panOptions.add(panStepSize);

        JPanel panSettingsButtons = new JPanel();
        panSettingsButtons.setLayout(new BoxLayout(panSettingsButtons, BoxLayout.LINE_AXIS));
        JButton butSettings = createMoreSettingsButton();
        panSettingsButtons.add(butSettings);


        JPanel panSettings = new JPanel();
        panSettings.setLayout(new BoxLayout(panSettings, BoxLayout.Y_AXIS));

        panSettings.add(panOptions);
        panSettings.add(panSettingsButtons);
        panSettings.setBorder(new TitledBorder(null, "Settings",
                TitledBorder.LEADING, TitledBorder.TOP, null, null));

        gbc_opgui.gridx = 0;
        gbc_opgui.gridy = 0;
        mainPanel.add(panSettings, gbc_opgui);


        createButtonSaveARFF();
        createButtonSaveModel();
        createButtonLoadeModel();

        JPanel buttonPanel = createButtonPannel();

        createRoiTable();

        //The TableCellListener is responsible for forwarding changes of the ROI layer to the IQM gui. A normal
        //TableChangedEvent isn't sufficient because it provides only the new value. The custom TableCellListener
        //provides the new as well as the old value.
        TableCellListener tcl = new TableCellListener(roiTable, new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //Don't update the IQM-GUI if an external classifier is loaded
                if (!externalClassifierLoaded()) {
                    TableCellListener tcl = (TableCellListener) e.getSource();
                    int col = tcl.getColumn();
                    switch (col) {
                        case 0: //layer name changed
                            for (IDrawingLayer layer : ROILayerManager.getVisibleLayers()) {
                                if (layer.getName().equals(tcl.getOldValue())) {
                                    layer.setName(tcl.getNewValue().toString());
                                    layer.update();
                                    break; //To prevent changing multiple layer in case they have the same name
                                }
                            }
                            //Hacky workaround, please refactor me
                            ROILayerManager.replaceLayers((List<IDrawingLayer>) (List<?>) ROILayerManager.getAllLayers());
                            break;
                        case 1: //layer color changed
                            ROILayerDTO dto = roiTableModel.getDTOat(tcl.getRow());
                            for (DefaultDrawingLayer layer : ROILayerManager.getAllLayers()) {
                                if (layer.getName().equals(dto.getName())) {
                                    layer.setLayerColor((Color) tcl.getNewValue());
                                    layer.update();
                                    break; //To prevent changing multiple layer in case they have the same name
                                }
                            }
                            //Hacky workaround, please refactor me
                            ROILayerManager.replaceLayers((List<IDrawingLayer>) (List<?>) ROILayerManager.getAllLayers());
                            break;
                    }
                }
            }
        });

        createStatePanel();

        gbc_opgui.gridx = 0;
        gbc_opgui.gridy = 1;
        mainPanel.add(roiListScrollPane, gbc_opgui);

        gbc_opgui.gridx = 0;
        gbc_opgui.gridy = 2;
        mainPanel.add(buttonPanel, gbc_opgui);


        gbc_opgui.gridx = 0;
        gbc_opgui.gridy = GridBagConstraints.RELATIVE;
        gbc_opgui.fill = GridBagConstraints.HORIZONTAL;

        mainPanel.add(statePanel, gbc_opgui);


        this.pack();

        //The main function of this thread is to make the Operator GUI continuously update itself. This is necessary to
        //keep the DrawingLayers consistent with the IQM GUI.
        updateThread = new Thread(new PluginGUIUpdater(this));
        updateThread.start();

        popup = new OptionsPopup(this, "Message");
        popup.pack();
    }

    @Override
    public void setWorkPackage(IWorkPackage workPackage) {
        super.setWorkPackage(workPackage);

        //Cannot be added into constructor because operator is not yet initialized at this point
        getOperator().addPropertyChangeListener(this);
    }

    private JButton createMoreSettingsButton() {
        JButton butSettings = new JButton(I18N.getMessage("gui.btn.settings"));
        butSettings.setToolTipText(I18N.getMessage("gui.btn.settings.tooltip"));
        butSettings.setActionCommand(OP_SEMIAUTO.MORE_SETTINGS.pbKey());
        butSettings.addActionListener(this);
        return butSettings;
    }

    private JPanel createStepSizePanel() {
        JPanel panStepSize = new JPanel(new FlowLayout());
        JLabel lblScaling = new JLabel(
                I18N.getMessage("stepsize"));
        createStepSizeSpinner();
        lblScaling.setLabelFor(spStepSize);
        panStepSize.add(lblScaling);
        panStepSize.add(spStepSize);
        return panStepSize;
    }

    private JPanel createWindowSizePanel() {
        JLabel lblWindowSizeSpinner = new JLabel(
                I18N.getMessage("windowsize"));
        createWindowSizeSpinner(lblWindowSizeSpinner);
        JPanel panWindowSize = new JPanel(new FlowLayout());
        panWindowSize.add(lblWindowSizeSpinner);
        panWindowSize.add(spWindowSize);
        return panWindowSize;
    }

    private JPanel createMainPanel() {
        JPanel mainPanel = new JPanel();
        getOpGUIContent().add(mainPanel);
        mainPanel.setLayout(new GridBagLayout());
        return mainPanel;
    }

    private JPanel createButtonPannel() {
        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 0;
        c.gridy = 0;
        buttonPanel.add(buttSaveARFF, c);
        c.gridx = 1;
        c.gridy = 0;
        buttonPanel.add(buttSaveModel, c);
        c.gridx = 2;
        c.gridy = 0;
        buttonPanel.add(buttLoadModel, c);
        return buttonPanel;
    }

    private void createStepSizeSpinner() {
        spStepSize = new JSpinner();
        spStepSize.addChangeListener(this);
        spStepSize.setModel(new SpinnerNumberModel(getDefaultValue(OP_SEMIAUTO.STEP.pbKey()), 1, 15, 1));
    }

    private void createWindowSizeSpinner(JLabel lblWindowSizeSpinner) {
        spWindowSize = new JSpinner();
        spWindowSize.addChangeListener(this);
        spWindowSize.setSize(300, 0);
        lblWindowSizeSpinner.setLabelFor(spWindowSize);
        spWindowSize.setModel(new SpinnerNumberModel(getDefaultValue(OP_SEMIAUTO.WINDOW_SIZE.pbKey()), 3,
                100, 2));
    }

    private void createRoiTable() {
        roiTableModel = new ROITableModel();
        roiTableModel.addTableModelListener(this);

        roiTable = new JTable(roiTableModel) {
            public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
                Component c = super.prepareRenderer(renderer, row, column);
                if (!isCellEditable(row, column)) {
                    c.setBackground(Color.LIGHT_GRAY);
                }
                return c;
            }
        };
        roiTable.setDefaultEditor(Color.class, new ColorEditor());
        roiTable.setDefaultRenderer(Color.class, new ColorRenderer(true));
        roiTable.setRowSelectionAllowed(false);


        roiListScrollPane = new JScrollPane(roiTable);
        roiListScrollPane.setBorder(new TitledBorder(null, "Classes",
                TitledBorder.LEADING, TitledBorder.TOP, null, null));
        roiTable.setPreferredScrollableViewportSize(new Dimension((int) roiListScrollPane.getBounds().getWidth(), 120));

        tableRowSorter = new TableRowSorter<>(roiTableModel);
        roiTable.setRowSorter(tableRowSorter);
    }

    private void createStatePanel() {
        statePanel = new JPanel();

        statePanel.setLayout(new GridBagLayout());
        statePanel.setBorder(new TitledBorder(null, "State",
                TitledBorder.LEADING, TitledBorder.TOP, null, null));
        GridBagConstraints c2 = new GridBagConstraints();
        c2.fill = GridBagConstraints.BOTH;
        c2.anchor = GridBagConstraints.NORTHWEST;

        lblStateText = new JLabel("Current State: ");
        c2.gridx = 0;
        c2.gridy = 0;
        lblStateText.setPreferredSize(new Dimension(80, 25));
        statePanel.add(lblStateText, c2);
        lblCurrentState = new JLabel(SemiautoSegmentationOp.State.READY.toString());
        c2.gridx = 1;
        c2.gridy = 0;
        lblCurrentState.setPreferredSize(new Dimension(200, 25));
        statePanel.add(lblCurrentState, c2);
        lblClsStateText = new JLabel("Classifier: ");
        c2.gridx = 0;
        c2.gridy = 1;
        lblClsStateText.setPreferredSize(new Dimension(80, 25));
        statePanel.add(lblClsStateText, c2);
        lblClsCurrentState = new JLabel();
        c2.gridx = 1;
        c2.gridy = 1;
        lblClsCurrentState.setPreferredSize(new Dimension(200, 25));
        statePanel.add(lblClsCurrentState, c2);
    }

    private void createButtonLoadeModel() {
        buttLoadModel = new JButton();
        buttLoadModel.setText(I18N.getMessage("gui.btn.loadmodel"));
        buttLoadModel.setToolTipText(I18N.getMessage("gui.btn.loadmodel.tooltip"));
        buttLoadModel.addActionListener(this);
        buttLoadModel.setActionCommand(OP_SEMIAUTO.LOAD_MODEL.pbKey());
        buttLoadModel.setPreferredSize(new Dimension(100, 30));
    }

    private void createButtonSaveARFF() {
        buttSaveARFF = new JButton();
        buttSaveARFF.setText(I18N.getMessage("gui.btn.savearff"));
        buttSaveARFF.setToolTipText(I18N.getMessage("gui.btn.savearff.tooltip"));
        buttSaveARFF.addActionListener(this);
        buttSaveARFF.setActionCommand(OP_SEMIAUTO.SAVE_ARFF.pbKey());
        buttSaveARFF.setPreferredSize(new Dimension(100, 30));
        buttSaveARFF.setEnabled(false);
    }

    private void createButtonSaveModel() {
        buttSaveModel = new JButton();
        buttSaveModel.setText(I18N.getMessage("gui.btn.savemodel"));
        buttSaveModel.setToolTipText(I18N.getMessage("gui.btn.savemodel.tooltip"));
        buttSaveModel.addActionListener(this);
        buttSaveModel.setActionCommand(OP_SEMIAUTO.SAVE_MODEL.pbKey());
        buttSaveModel.setPreferredSize(new Dimension(100, 30));
        buttSaveModel.setEnabled(false);
    }

    @Override
    public void destroy() {
        updateThread.interrupt();
        super.destroy();

    }

    @Override
    public void setParameterValuesToGUI() {
        // the first statement here MUST be the following line
        this.pb = this.workPackage.getParameters();

        // remove the listeners, if any before setting the spWindowSize
        spWindowSize.removeChangeListener(this);
        spWindowSize.setValue(this.pb.getIntParameter(OP_SEMIAUTO.WINDOW_SIZE.pbKey()));
        spWindowSize.addChangeListener(this);

        spStepSize.removeChangeListener(this);
        spStepSize.setValue(this.pb.getIntParameter(OP_SEMIAUTO.STEP.pbKey()));
        spStepSize.addChangeListener(this);

    }

    @Override
    public void updateParameterBlock() {
        // updates the parameter block according to the current GUI element
        // settings

        this.pb.setParameter(OP_SEMIAUTO.WINDOW_SIZE.pbKey(), ((Number) spWindowSize.getValue()).intValue());
        this.pb.setParameter(OP_SEMIAUTO.STEP.pbKey(), ((Number) spStepSize.getValue()).intValue());
        this.pb.setParameter(OP_SEMIAUTO.LAYERS.pbKey(), roiTableModel.getSelectedLayers());
        //resultimages and classifier are updated elsewhere
    }

    @Override
    public void update() {
        fetchROILayers();

        if (externalClassifierLoaded()) {
            lblClsCurrentState.setText("Classifier loaded");
            lblClsCurrentState.setForeground(Color.BLUE);
        } else {
            lblClsCurrentState.setText("New classifier will be trained");
            lblClsCurrentState.setForeground(Color.BLACK);
        }
    }

    private void fetchROILayers() {
        if (externalClassifierLoaded()) {
            roiTableModel.setEditableIndices(Arrays.asList(1));
            List<String> clsClassNames = WekaFileUtil.getClsClassNames(pb.getObjectParameter(OP_SEMIAUTO.CLASSIFIER.pbKey()));
            List<Color> clsClassColors = WekaFileUtil.getClsClassColors(pb.getObjectParameter(OP_SEMIAUTO.CLASSIFIER.pbKey()));
            if (clsClassColors.size() != clsClassNames.size()) {
                if (clsClassColors.size() == clsClassNames.size() - 1) {
                    clsClassColors.add(SemiautoSegmentationOp.getDefaultColor(-1));
                } else {
                    for (int i = 0; i < clsClassNames.size(); i++) {
                        clsClassColors.add(SemiautoSegmentationOp.getDefaultColor(i));
                    }
                }

            }

            boolean updateNeeded = false;
            if (roiTableModel.getRowCount() != clsClassNames.size()) {
                updateNeeded = true;
            } else for (ROILayerDTO dto : roiTableModel.getData()) {
                if (!clsClassNames.contains(dto.getName())) {
                    updateNeeded = true;
                }
            }
            if (updateNeeded) {
                roiTableModel.clear();
                for (String name : clsClassNames) {
                    roiTableModel.addRow(new ROILayerDTO(name, clsClassColors.get(clsClassNames.indexOf(name)), "", new ArrayList<ROIShape>()));
                }
                //Hide the "?" layer from the view
                tableRowSorter.setRowFilter(new RowFilter<ROITableModel, Integer>() {
                    @Override
                    public boolean include(Entry<? extends ROITableModel, ? extends Integer> entry) {
                        ROITableModel model = entry.getModel();
                        ROILayerDTO dto = model.getDTOat(entry.getIdentifier());
                        return !dto.getName().equals(WekaClassifier.NONE_VALUE);
                    }
                });
            }


        } else {
            roiTableModel.setEditableIndices(Arrays.asList(0, 1, 2));
            List<String> unselectedItems = new ArrayList<>();
            for (ROILayerDTO dto : roiTableModel.getData()) {
                if (!dto.isSelected()) {
                    unselectedItems.add(dto.getName());
                }
            }
            roiTableModel.clear();
            List<IDrawingLayer> layerList = ROILayerManager.getVisibleLayers();
            for (IDrawingLayer layer : layerList) {
                ROILayerDTO dto = new ROILayerDTO(layer.getName(), layer.getLayerColor(), "", layer.getAllROIShapes());
                if (unselectedItems.contains(dto.getName())) {
                    dto.setSelected(false);
                }
                roiTableModel.addRow(0, dto);
            }
        }
    }

    /**
     * Returns whether or not an external classifier was loaded into the paramater block
     *
     * @return
     */
    private boolean externalClassifierLoaded() {
        return this.pb.getObjectParameter(OP_SEMIAUTO.CLASSIFIER.pbKey()) != null;
    }

    @Override
    public void reset() {
        super.reset();
        tableRowSorter.setRowFilter(null);
        getOperator().reset();
        roiTableModel.clear();
        //this.pb.setParameter(OP_SEMIAUTO.RESULT_IMAGES.pbKey(), SemiautoSegmentationOp.getAvailableImageTypes());
        //this.pb = new ParameterBlockIQM(new SemiautoSegmentationOpDescriptor());
        this.popup.resetSelection();

        buttLoadModel.setText(I18N.getMessage("gui.btn.loadmodel"));
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case "save_arff":
                saveArff();
                break;
            case "save_model":
                saveModel();
                break;
            case "load_model":
                loadOrDeleteModel();
                break;
            case "more_settings":
                openOrClosePopupDialog();
                break;
        }

    }

    private void openOrClosePopupDialog() {
        popup.setLocationRelativeTo(this);
        popup.setVisible(!popup.isVisible());
    }

    private void saveModel() {
        try {

            File clsLocation = GuiUtils.showFileDialog(JFileChooser.SAVE_DIALOG);
            if (clsLocation != null) {
                getOperator().saveTrainedModel(clsLocation);
            }
        } catch (Exception e) {
            e.printStackTrace();
            DialogUtil.getInstance().showErrorMessage(
                    "Saving external classifier failed!",
                    e);
        }
    }

    private void loadOrDeleteModel() {
        if (!externalClassifierLoaded()) {
            try {
                Object cls = null;
                File clsLocation = GuiUtils.showFileDialog(JFileChooser.OPEN_DIALOG);
                if (clsLocation != null) {
                    cls = WekaFileUtil.loadModel(clsLocation);
                    this.pb.setParameter(OP_SEMIAUTO.CLASSIFIER.pbKey(), cls);
                    buttLoadModel.setText(I18N.getMessage("gui.btn.removemodel"));
                    buttLoadModel.setToolTipText(I18N.getMessage("gui.btn.removemodel.tooltip"));
                }
            } catch (Exception e) {
                e.printStackTrace();
                DialogUtil.getInstance().showErrorMessage(
                        "Loading external classifier failed!",
                        e);

            }
        } else {
            this.pb.setParameter(OP_SEMIAUTO.CLASSIFIER.pbKey(), null);
            buttLoadModel.setText(I18N.getMessage("gui.btn.loadmodel"));
            buttLoadModel.setToolTipText(I18N.getMessage("gui.btn.loadmodel.tooltip"));
        }
    }

    private void saveArff() {
        getOperator().saveArffFiles(GuiUtils.showDirectoryDialog());
        //getOperator().saveArffFiles(new File("."));
    }

    @Override
    public void stateChanged(ChangeEvent e) {
        // update the parameter block each time the spWindowSize changes
        this.updateParameterBlock();
    }

    /**
     * Returns a reference to the operator. Cannot be used in the constructor of the GUI because then the the operator is not yet initialized
     *
     * @return
     */
    private SemiautoSegmentationOp getOperator() {
        return (SemiautoSegmentationOp) Application.getCurrentExecutionProtocol().getWorkPackage().getOperator();
    }

    private Number getDefaultValue(String paramKey) {
        SemiautoSegmentationOpDescriptor desc = new SemiautoSegmentationOpDescriptor();

        Number value = -1;
        ArrayList<String> paramNames = new ArrayList(Arrays.asList(desc.getParamNames()));
        if (paramNames.contains(paramKey)) {
            int index = paramNames.indexOf(paramKey);
            Object obj = desc.getParamDefaults()[index];
            if (obj instanceof Number) {
                value = (Number) obj;
            }
        }
        return value;
    }

    @Override
    public void tableChanged(TableModelEvent e) {
        //Update the selection of result images
        if (e.getSource() instanceof ImgTableModel) {
            ImgTableModel model = (ImgTableModel) e.getSource();
            this.pb.setParameter(OP_SEMIAUTO.RESULT_IMAGES.pbKey(), model.getSelectedImageTypes());
        } else if (e.getSource() instanceof ROITableModel) {
            updateParameterBlock();
        }
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        super.propertyChange(evt);

        //Process operator state change events
        if (evt.getSource().equals(getOperator()) && evt.getPropertyName().equals("currentState")) {
            lblCurrentState.setText(evt.getNewValue().toString());

            if (evt.getNewValue().equals(SemiautoSegmentationOp.State.DONE)) {
                buttSaveARFF.setEnabled(true);
                buttSaveModel.setEnabled(true);
            } else {
                buttSaveARFF.setEnabled(false);
                buttSaveModel.setEnabled(false);
            }
        }
    }
}
