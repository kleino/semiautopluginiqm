package at.fhjoanneum.eht.gui.table;


import javax.media.jai.ROIShape;
import javax.swing.*;
import java.awt.*;
import java.util.List;


/**
 * Created by Kleino on 06.05.2014.
 */
public class ROILayerDTO {
    private List<ROIShape> roiShapes;
    private String name;
    private String toolTip;
    private Color color;
    private Icon icon;
    private boolean selected = true;

    public ROILayerDTO(String name, Color color, String toolTip, List<ROIShape> roiShapes) {
        this.name = name;
        this.color = color;
        this.toolTip = toolTip;
        this.roiShapes = roiShapes;
    }

    public ROILayerDTO(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public String getToolTip() {
        return toolTip;
    }

    public void setToolTip(String toolTip) {
        this.toolTip = toolTip;
    }

    public void setIcon(Icon icon) {
        this.icon = icon;
    }

    public List<ROIShape> getRoiShapes() {
        return roiShapes;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public Object getValueAt(int columnIndex) {
        Object value = "??";
        switch (columnIndex) {
            case 0:
                value = this.getName();
                break;
            case 1:
                value = this.getColor();
                break;
            case 2:
                value = this.isSelected();
                break;
        }
        return value;
    }

    public void setValueAt(Object aValue, int columnIndex) {
        switch (columnIndex) {
            case 0:
                this.setName(aValue.toString());
                break;
            case 1:
                this.setColor((Color) aValue);
                break;
            case 2:
                this.setSelected((boolean) aValue);
                break;
        }
    }
}
