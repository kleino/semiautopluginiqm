package at.fhjoanneum.eht.gui.table;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Vector;

/**
 * Created by Kleino on 26.07.2014.
 */
public class ROITableModel extends AbstractTableModel {
    private Vector<ROILayerDTO> roiLayerDTOs;
    private Vector<String> columnNames;


    private List<Integer> editableIndices = Arrays.asList(0, 1, 2);

    public ROITableModel() {
        this.roiLayerDTOs = new Vector<>();
        this.columnNames = new Vector<>(Arrays.asList("Classname", "Color", "Selected"));
    }

    @Override
    public String getColumnName(int column) {
        return columnNames.get(column);
    }

    @Override
    public int getRowCount() {
        return roiLayerDTOs.size();
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        if (roiLayerDTOs.size() > rowIndex) {
            return roiLayerDTOs.get(rowIndex).getValueAt(columnIndex);
        } else {
            return new Object();
        }
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        if (aValue != null && !aValue.toString().trim().isEmpty()) {
            roiLayerDTOs.get(rowIndex).setValueAt(aValue, columnIndex);
            this.fireTableDataChanged();
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        if (editableIndices.contains(columnIndex)) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return getValueAt(0, columnIndex).getClass();
    }

    public void addRow(ROILayerDTO dto) {
        roiLayerDTOs.add(dto);
        this.fireTableDataChanged();
    }

    public void addRow(int index, ROILayerDTO dto) {
        roiLayerDTOs.add(index, dto);
        this.fireTableDataChanged();
    }

    public Vector<ROILayerDTO> getData() {
        return roiLayerDTOs;
    }

    public void setEditableIndices(List<Integer> editableIndices) {
        this.editableIndices = editableIndices;
    }

    public ROILayerDTO getDTOat(int index) {
        return roiLayerDTOs.get(index);
    }

    public void clear() {
        roiLayerDTOs = new Vector<>();
    }

    public List<ROILayerDTO> getSelectedLayers() {
        List<ROILayerDTO> selectedROILayers = new ArrayList<>();
        for (ROILayerDTO dto : getData()) {
            if (dto.isSelected()) {
                selectedROILayers.add(dto);
            }
        }
        return selectedROILayers;
    }
}
