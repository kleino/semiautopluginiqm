package at.fhjoanneum.eht.gui;

import at.mug.iqm.api.operator.AbstractImageOperatorGUI;

import javax.swing.*;

/**
 * Created by jürgen on 08.05.2014.
 */
public class PluginGUIUpdater implements Runnable {

    private final AbstractImageOperatorGUI component;

    public PluginGUIUpdater(AbstractImageOperatorGUI component) {
        this.component = component;
    }

    @Override
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    component.update();
                }
            });
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }


    }
}
