package at.fhjoanneum.eht.gui.popup;

import at.fhjoanneum.eht.gui.SemiautoSegmentationOpGUI;
import at.mug.iqm.api.gui.util.TableColumnAdjuster;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.event.TableModelListener;
import java.awt.*;

/**
 * Created by Kleino on 27.07.2014.
 */
public class OptionsPopup extends JDialog {
    TableModelListener parent;
    private ImgTableModel model;


    public OptionsPopup(SemiautoSegmentationOpGUI owner, String title) {
        super(owner, title);
        parent = owner;


        JPanel resultImagePanel = createResultImagePanel();

        Border padding = BorderFactory.createEmptyBorder(2, 2, 2, 2);
        resultImagePanel.setBorder(padding);

        JTabbedPane tabbedPane = new JTabbedPane();
        tabbedPane.addTab("Result images", null, resultImagePanel, "Result images");
        add(tabbedPane, BorderLayout.CENTER);


    }

    private JPanel createResultImagePanel() {
        JPanel resultImagePanel = new JPanel();
        model = new ImgTableModel();
        model.addTableModelListener(parent);

        JTable table = new JTable(model);


        table.setRowSelectionAllowed(false);
        JScrollPane scrollPane = new JScrollPane(table);
        resultImagePanel.add(scrollPane, BorderLayout.CENTER);
        //resultImagePanel.setSize(200, 150);
        TableColumnAdjuster tca = new TableColumnAdjuster(table);
        tca.adjustColumns();

        table.setPreferredScrollableViewportSize(new Dimension(150, 120));
        return resultImagePanel;
    }

    public void resetSelection() {
        model.removeTableModelListener(parent);
        model.selectEverything();
        model.addTableModelListener(parent);
        model.fireTableDataChanged();

    }
}
