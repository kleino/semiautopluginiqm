package at.fhjoanneum.eht.gui.popup;

import at.fhjoanneum.eht.SemiautoSegmentationOp;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Vector;

/**
 * Created by Kleino on 27.07.2014.
 */
public class ImgTableModel extends AbstractTableModel {
    Object rowData[][];
    Vector<String> colNames = new Vector<>(Arrays.asList("Image type", "Selected"));

    public ImgTableModel() {
        super();
        List<String> imtTypes = SemiautoSegmentationOp.getAvailableImageTypes();
        rowData = new Object[imtTypes.size()][2];
        for (int i = 0; i < imtTypes.size(); i++) {
            rowData[i] = new Object[]{imtTypes.get(i), true};
        }
    }

    @Override
    public int getRowCount() {
        return rowData.length;
    }

    @Override
    public int getColumnCount() {
        return colNames.size();
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        return rowData[rowIndex][columnIndex];
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        rowData[rowIndex][columnIndex] = aValue;
        this.fireTableDataChanged();
    }

    @Override
    public Class getColumnClass(int column) {
        return (getValueAt(0, column).getClass());
    }

    @Override
    public String getColumnName(int column) {
        return colNames.get(column);
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return (columnIndex != 0);
    }

    public List<String> getSelectedImageTypes() {
        List<String> selectedImageTypes = new ArrayList<>();
        for (int i = 0; i < getRowCount(); i++) {
            if ((boolean) getValueAt(i, 1)) {
                selectedImageTypes.add((String) getValueAt(i, 0));
            }
        }
        return selectedImageTypes;
    }

    public void selectEverything() {
        for (int i = 0; i < getRowCount(); i++) {
            setValueAt(true, i, 1);
        }
    }
}
