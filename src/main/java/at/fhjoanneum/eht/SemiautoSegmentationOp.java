package at.fhjoanneum.eht;

import at.fhjoanneum.eht.calc.IDataCalculationStrategy;
import at.fhjoanneum.eht.calc.ParallelCalculationStrategyInstantSingleWindow;
import at.fhjoanneum.eht.gui.table.ROILayerDTO;
import at.fhjoanneum.eht.params.OP_SEMIAUTO;
import at.fhjoanneum.eht.utils.Utils;
import at.fhjoanneum.eht.weka.WekaClassifier;
import at.fhjoanneum.eht.weka.WekaFileUtil;
import at.mug.iqm.api.model.CustomDataModel;
import at.mug.iqm.api.model.IqmDataBox;
import at.mug.iqm.api.model.TableModel;
import at.mug.iqm.api.operator.*;
import at.mug.iqm.commons.util.image.ImageTools;
import org.apache.log4j.Logger;
import weka.core.Attribute;
import weka.core.Instances;

import javax.media.jai.PlanarImage;
import javax.media.jai.ROIShape;
import javax.media.jai.RasterFactory;
import javax.media.jai.TiledImage;
import java.awt.*;
import java.awt.image.ColorModel;
import java.awt.image.DataBuffer;
import java.awt.image.SampleModel;
import java.awt.image.WritableRaster;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.File;
import java.util.*;
import java.util.List;

/**
 * This class represents the algorithm of this IQM operator.
 *
 * @author Jürgen Kleinowitz
 */
public class SemiautoSegmentationOp extends AbstractOperator implements Observer {
    private static final Logger logger = Logger.getLogger(SemiautoSegmentationOp.class);
    private final static List<String> ALL_AVAILABLE_IMG_TYPES = Arrays.asList("Overlaid image", "Class Image", "Feature Images", "Work image", "Evaluation result");
    private int windowSize;
    private int stepSize;
    private List<String> classNames;
    private List<String> defaultClassNames = new ArrayList<>();
    private List<Color> classColors;
    private int scaledWidth;
    private int scaledHeight;
    private PlanarImage baseImage;
    private PlanarImage workImage;
    private TableModel allImageData;
    private WekaClassifier wekaClassifier;
    private Object loadedCls;
    private boolean paramsChanged = true; //Determines wheter Data needs to be recalculated
    private Result result;
    private State currentState = State.READY;
    private List<ROILayerDTO> roiLayers;
    private IDataCalculationStrategy calculationStrategy = new ParallelCalculationStrategyInstantSingleWindow();
    private List<String> imgTypes;

    private PropertyChangeSupport statePcs = new PropertyChangeSupport(this);

    public static List<String> getAvailableImageTypes() {
        return ALL_AVAILABLE_IMG_TYPES;
    }

    public static Color getDefaultColor(int index) {
        float hue = (float) index / 6;
        return Color.getHSBColor(hue, 1, 1);
    }

    public void reset() {
        changeState(State.READY);
        loadedCls = null;
        allImageData = null;
        classNames = null;
        classColors = null;
        workImage = null;
        baseImage = null;

        paramsChanged = true;
    }

    @Override
    public void update(Observable o, Object arg) {
        int progress = (int) ((float) arg * 100);
        fireProgressChanged(progress);

    }

    /**
     * Gets the unique name of the operator.
     * <p/>
     * This method is merely a wrapper for the unique name of the operator
     * stored in the operator's associated {@link IOperatorDescriptor}.
     *
     * @return the name of the operator, declared in the operator's descriptor
     */
    public String getName() {
        if (this.name == null) {
            this.name = new SemiautoSegmentationOpDescriptor().getName();
        }
        return this.name;
    }

    /**
     * Gets the {@link OperatorType} of the operator.
     * <p/>
     * This method is merely a wrapper for the type of the operator stored in
     * the operator's associated {@link IOperatorDescriptor}.
     *
     * @return the type of the operator, declared in the operator's descriptor
     */
    public OperatorType getType() {
        if (this.type == null) {
            this.type = SemiautoSegmentationOpDescriptor.TYPE;
        }
        return this.type;
    }

    /**
     * This is the starting point for the execution of the operator.
     * <p/>
     * The algorithm code is launched using the {@link #run(IWorkPackage)}
     * method and returns a {@link IResult}.
     *
     * @param wp the work package for this algorithm
     * @return a result of the processed work package
     */
    @Override
    public IResult run(IWorkPackage wp) throws Exception {

        changeState(State.VALIDATION);

        if (!new SemiautoSegmentationOpValidator().validateExecution(wp)) {
            //This if clause is never reached because the validator already throws an exception and only ever returns true if anything
            changeState(State.ERROR);
            throw new IllegalArgumentException("Validation error");
        }

        changeState(State.START);
        result = new Result();

        logger.debug("Starting SemiauotSegmentationOp");

        retrieveParams(wp.getParameters());

        if (needsRecalculation()) {
            changeState(State.CALCULATING);

            //Add Border and convert to greyscale
            workImage = doImagePreProcessing(baseImage);

            //Calculate the statistical values
            allImageData = createFullImageStatistics(workImage);
            //Remove unnecessary columns
            //TableUtils.removeColums(allImageData, new int[]{0, 1, 2, 3, 4, 5, 20, 21, 22});
            //TableUtils.removeColums(allImageData, new int[]{1, 2, 3, 4, 5, 6, 21, 22, 23});
        }

        getClassNamesAndColors(roiLayers);
        if (externalClassifierLoaded()) {
            changeState(State.EXTERNAL_CLS);

            //Only create classification set and use already trained classifier
            wekaClassifier = new WekaClassifier(allImageData, loadedCls);
            //WekaFileUtil.saveAllInstances(new File("."), wekaClassifier);
        } else {
            changeState(State.TRAINING);

            //Assign class values based on ROIs
            assignClassValue();
            //Create all data and train classifier
            fireProgressChanged(90);
            wekaClassifier = new WekaClassifier(allImageData, classNames);

            //WekaFileUtil.saveAllInstances(new File("."), wekaClassifier);
        }
        changeState(State.CLASSIFYING);
        wekaClassifier.useClassifier();


        PlanarImage resImage = createResultImage(wekaClassifier.getInstancesObject(WekaClassifier.CLASSIFIED_SET));
        PlanarImage overlayedImage = Utils.createOverlayImage(baseImage, resImage);

        for (int i = 0; i < getAvailableImageTypes().size(); i++) {
            if (imgTypes.contains(getAvailableImageTypes().get(i))) {
                switch (i) {
                    case 0: //Overlaid image
                        result.addItem(Utils.createIqmDataBox(overlayedImage, overlayedImage.getProperty("model_name").toString() + "_overlay"));
                        break;
                    case 1: //Class image
                        result.addItem(Utils.createIqmDataBox(resImage, resImage.getProperty("model_name").toString()));
                        break;
                    case 2: //Feature images
                        createAndAddFeatureImages(result, wekaClassifier.getInstancesObject(WekaClassifier.CLASSIFICATION_SET));
                        break;
                    case 3: //Work image
                        result.addItem(Utils.generateNewImageModel(workImage, "work_image"));
                        break;
                    case 4: //Evaluation result
                        if (!externalClassifierLoaded()) {
                            CustomDataModel cdm = new CustomDataModel();
                            String parameterString = "StepSize: " + stepSize + ", WindowSize: " + windowSize + "\n";
                            cdm.setContent(new Object[]{wekaClassifier.getEvaluationResult() + parameterString});
                            IqmDataBox box = new IqmDataBox(cdm);
                            result.addItem(box);
                        }

                        break;
                }

            }
        }
        changeState(State.DONE);
        fireProgressChanged(100);
        return result;
    }

    private PlanarImage doImagePreProcessing(PlanarImage img) {
        //TODO: Resizing maybe?
        //img = resizeImage(0.5f, img);

        img = Utils.addBorder(img, windowSize / 2);
        //String baseImgName = baseImage.getProperty("image_name").toString();
        //String baseFileName = baseImage.getProperty("file_name").toString();

        /*double numBands = img.getNumBands();
        if (numBands > 1) {
            img = toGrayScale(img);
        } */
        return img;
    }

    public boolean externalClassifierLoaded() {
        return loadedCls != null;
    }

    private boolean needsRecalculation() {
        return allImageData == null || paramsChanged;
    }

    /**
     * Retrieve param values. If they have changed, paramsChanged will be set to true
     *
     * @param pb
     */
    private void retrieveParams(ParameterBlockIQM pb) {
        int windowSizeOld = windowSize;
        int stepSizeOld = stepSize;
        String imgHashOld;
        if (baseImage == null) {
            imgHashOld = "";
        } else {
            imgHashOld = Utils.calcImageHash(baseImage);
        }

        windowSize = pb.getIntParameter(OP_SEMIAUTO.WINDOW_SIZE.pbKey());
        stepSize = pb.getIntParameter(OP_SEMIAUTO.STEP.pbKey());
        baseImage = ((IqmDataBox) pb.getSource(0)).getImage();
        roiLayers = (List<ROILayerDTO>) pb.getObjectParameter(OP_SEMIAUTO.LAYERS.pbKey());
        imgTypes = (List<String>) pb.getObjectParameter(OP_SEMIAUTO.RESULT_IMAGES.pbKey());

        loadedCls = pb.getObjectParameter(OP_SEMIAUTO.CLASSIFIER.pbKey());

        String imgHashNew = Utils.calcImageHash(baseImage);

        //If any of these parameters change, data needs to be recalulated
        if (windowSizeOld != windowSize || stepSizeOld != stepSize || !imgHashOld.equals(imgHashNew)) {
            paramsChanged = true;
        } else {
            paramsChanged = false;
        }
    }

    private void assignClassValue() {
        List<ROIShape> combinedLayershapes = getCombinedLayerShapes(roiLayers);
        for (int index = 0; index < allImageData.getRowCount(); index++) {
            int i = (index / scaledHeight) * stepSize;
            int j = (index % scaledHeight) * stepSize;

            String classVal = "";
            for (ROIShape combinedShape : combinedLayershapes) {
                if (combinedShape.contains(i, j)) {
                    classVal = classNames.get(combinedLayershapes.indexOf(combinedShape));
                }
                if (classVal.isEmpty()) {
                    classVal = WekaClassifier.NONE_VALUE;
                }
            }
            allImageData.setValueAt(classVal, index, allImageData.getColumnCount() - 1);
        }
    }

    private void createAndAddFeatureImages(Result res, Instances instancesObject) {

        int numAttr = instancesObject.numAttributes();
        int numRows = instancesObject.numInstances();
        HashMap<String, ArrayList<Double>> columnValues = new HashMap<>();
        for (int i = 0; i < numRows; i++) {
            for (int j = 0; j < numAttr; j++) {
                String key = instancesObject.attribute(j).name();
                Double attrVal = instancesObject.instance(i).value(j);
                if (!columnValues.containsKey(key)) {
                    ArrayList<Double> doubeList = new ArrayList<>();
                    doubeList.add(attrVal);
                    columnValues.put(key, doubeList);
                } else {
                    columnValues.get(key).add(attrVal);
                }
            }
        }
        for (Map.Entry<String, ArrayList<Double>> entry : columnValues.entrySet()) {
            PlanarImage pi = createImage(entry.getValue());
            res.addItem(Utils.createIqmDataBox(pi, entry.getKey()));
        }
    }

    private PlanarImage createImage(ArrayList<Double> featureValues) {
        if (scaledHeight * scaledWidth != featureValues.size()) {
            throw new IllegalArgumentException("Error: Something went terribly wrong");
        }

        TiledImage tiledImage = createEmptyTiledImage(scaledWidth, scaledHeight, 1);
        WritableRaster raster = tiledImage.getWritableTile(0, 0);

        for (int i = 0; i < scaledWidth; i++) {
            for (int j = 0; j < scaledHeight; j++) {
                int index = i * scaledHeight + j;
                Double d = featureValues.get(index) * 255;
                raster.setSample(i, j, 0, d);
            }
        }
        return resizeToBaseImage(baseImage, tiledImage);
    }

    private PlanarImage createResultImage(Instances instancesObject) {
        if (scaledHeight * scaledWidth != instancesObject.size()) {
            throw new IllegalArgumentException("Error: Something went terribly wrong");
        }
        TiledImage tiledImage = createEmptyTiledImage(scaledWidth, scaledHeight, 3);
        WritableRaster raster = tiledImage.getWritableTile(0, 0);
        Attribute classAttr = instancesObject.classAttribute();

        for (int i = 0; i < scaledWidth; i++) {
            for (int j = 0; j < scaledHeight; j++) {
                int index = i * scaledHeight + j;
                Double d = instancesObject.instance(index).classValue();
                Color col = getColor(classAttr, d);
                raster.setSample(i, j, 0, col.getRed());
                raster.setSample(i, j, 1, col.getGreen());
                raster.setSample(i, j, 2, col.getBlue());
            }
        }
        return resizeToBaseImage(baseImage, tiledImage);
    }

    private Color getColor(Attribute classAttr, Double d) {
        String classVal = classAttr.value(d.intValue());

        int index;
        if (classNames.contains(classVal)) {
            index = classNames.indexOf(classVal);
        } else {
            index = d.intValue() - 1;
        }
        return classColors.get(index);
    }

    private TiledImage createEmptyTiledImage(int width, int height, int bands) {
        SampleModel sampleModel = RasterFactory.createBandedSampleModel(DataBuffer.TYPE_BYTE,
                width, height, bands);
        ColorModel cm = TiledImage.createColorModel(sampleModel);
        return new TiledImage(0, 0, scaledWidth, scaledHeight, 0, 0, sampleModel, cm);
    }

    private PlanarImage resizeToBaseImage(PlanarImage baseImage, TiledImage tiledImage) {
        float scaleW = (float) baseImage.getWidth() / scaledWidth;
        PlanarImage resizedImage = Utils.resizeImage(scaleW, tiledImage);
        resizedImage.setProperty("model_name", "W:" + windowSize + "_S" + stepSize);
        resizedImage.setProperty("image_name", "W:" + windowSize + "_S" + stepSize);
        return resizedImage;
    }

    private void getClassNamesAndColors(List<ROILayerDTO> drawingLayers) {
        classColors = new ArrayList<>();
        classNames = new ArrayList<>();


        //classColors.add(Color.WHITE);

        for (ROILayerDTO layer : drawingLayers) {
            classNames.add(layer.getName());
            classColors.add(layer.getColor());
        }
        //
        classNames.add(WekaClassifier.NONE_VALUE);
    }

    private TableModel createFullImageStatistics(PlanarImage pi) throws Exception {
        int startValue;
        int endValueX;
        int endValueY;

        startValue = 0;
        endValueX = pi.getTileWidth() * pi.getMaxTileX();
        endValueY = pi.getTileHeight() * pi.getMaxTileY();

        scaledWidth = (int) Math.ceil(((double) endValueX - (double) startValue) / (double) stepSize);
        scaledHeight = (int) Math.ceil(((double) endValueY - (double) startValue) / (double) stepSize);
        calculationStrategy.addObserver(this);

        return calculationStrategy.calculate(pi, windowSize, stepSize, startValue, endValueX, endValueY);


    }

    /**
     * Combine all roi shapes of a drawing layer to a single ROI
     *
     * @param drawingLayers
     * @return
     */
    private List<ROIShape> getCombinedLayerShapes(List<ROILayerDTO> drawingLayers) {
        List<ROIShape> combinedLayershapes = new ArrayList<>();
        for (ROILayerDTO layer : drawingLayers) {
            ROIShape combinedShape = new ROIShape(new Rectangle(0, 0, 0, 0));
            for (ROIShape shape : layer.getRoiShapes()) {
                combinedShape = (ROIShape) combinedShape.add(shape);
            }
            combinedLayershapes.add(combinedShape);
        }
        return combinedLayershapes;
    }

    private PlanarImage toGrayScale(PlanarImage pi) {
        logger.debug("NumBands > 1, converting to grayscale image");

        List<Integer> bands = new ArrayList<Integer>();
        for (int i = 0; i < pi.getNumBands(); i++) {
            bands.add(i);
        }
        return ImageTools.combineChannels(pi, bands);
    }

    public void saveTrainedModel(File file) throws Exception {
        if (currentState.equals(State.DONE)) {
            WekaFileUtil.saveModel(file, wekaClassifier.getCls(), wekaClassifier.getClsInputFormat(), classColors.toArray(new Color[classColors.size()]));
        }
    }

    public void saveArffFiles(File file) {
        if (currentState.equals(State.DONE)) {
            WekaFileUtil.saveAllInstances(file, wekaClassifier);
        }
    }

    public State getCurrentState() {
        return currentState;
    }

    public IDataCalculationStrategy getCalculationStrategy() {
        return calculationStrategy;
    }

    public void setCalculationStrategy(IDataCalculationStrategy calculationStrategy) {
        this.calculationStrategy = calculationStrategy;
    }

    public List<Color> getClassColors() {
        if (classColors != null) {
            return classColors;
        } else {
            logger.error("ClassColors not yet set, returning empty list");
            return new ArrayList<>();
        }
    }

    public List<String> getClassNames() {
        if (classNames != null) {
            return classNames;
        } else {
            logger.error("ClassNames not yet set, returning empty list");
            return new ArrayList<>();
        }
    }

    private void changeState(State newState) {
        State oldState = currentState;
        currentState = newState;
        statePcs.firePropertyChange("currentState", oldState, currentState);
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        statePcs.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        statePcs.removePropertyChangeListener(listener);
    }

    public enum State {READY, START, CALCULATING, CLASSIFYING, TRAINING, EXTERNAL_CLS, DONE, VALIDATION, ERROR}

}
