package at.fhjoanneum.eht.params;

/**
 * Created by Kleino on 09.05.2014.
 */
public enum OP_STATISTICS implements PARAM_BASE {

    ORDER2("Order2"), ORDER1("Order1"), BINARY("Binary");

    private String pbKey;

    OP_STATISTICS(String pbKey) {
        this.pbKey = pbKey;
    }

    @Override
    public String pbKey() {
        return pbKey;
    }

}
