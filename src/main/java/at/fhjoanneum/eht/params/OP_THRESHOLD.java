package at.fhjoanneum.eht.params;

/**
 * Created by Kleino on 09.05.2014.
 */
public enum OP_THRESHOLD {
    THRESHOLD_HIGH("ThresholdHigh1"),
    THRESHOLD_LOW("ThresholdLow1");

    private final String pbKey;

    OP_THRESHOLD(String pbKey) {
        this.pbKey = pbKey;
    }

    public String pbKey() {
        return pbKey;
    }
}
