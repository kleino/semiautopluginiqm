package at.fhjoanneum.eht.params;

/**
 * Created by Kleino on 09.05.2014.
 */
public enum OP_SEMIAUTO {
    ALL_LAYERS("AllLayers"),
    //BINARIZE("Binarize"),
    //OFFSET("Offset"),
    LAYERS("ROILayers"),
    WINDOW_SIZE("WindowSize"),
    STEP("Step"),
    SAVE_ARFF("save_arff"),
    SAVE_MODEL("save_model"),
    LOAD_MODEL("load_model"),
    CLASSIFIER("classifier"),
    PACKAGE_SIZE("package_size"),
    RESULT_IMAGES("result_images"), MORE_SETTINGS("more_settings");

    private final String pbKey;

    OP_SEMIAUTO(String pbKey) {
        this.pbKey = pbKey;
    }

    public String pbKey() {
        return pbKey;
    }
}
