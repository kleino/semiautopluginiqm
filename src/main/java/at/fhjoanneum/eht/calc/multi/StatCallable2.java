package at.fhjoanneum.eht.calc.multi;

import at.fhjoanneum.eht.params.OP_STATISTICS;
import at.mug.iqm.api.model.ImageModel;
import at.mug.iqm.api.model.IqmDataBox;
import at.mug.iqm.api.model.TableModel;
import at.mug.iqm.api.operator.ParameterBlockImg;
import at.mug.iqm.api.operator.WorkPackage;
import at.mug.iqm.img.bundle.descriptors.IqmOpStatisticsDescriptor;
import at.mug.iqm.img.bundle.op.IqmOpStatistics;

import javax.media.jai.PlanarImage;
import java.util.Vector;
import java.util.concurrent.Callable;

/**
 * Created by Kleino on 08.07.2014.
 */
public class StatCallable2 implements Callable<TableModel[]> {
    private final PlanarImage[] windowList;
    private final TableModel[] resultList;
    private final ParameterBlockImg statPb = new ParameterBlockImg(new IqmOpStatisticsDescriptor());
    private final IqmOpStatistics opStatistics = new IqmOpStatistics();
    private final WorkPackage thWp = new WorkPackage(opStatistics, statPb);

    public StatCallable2(PlanarImage[] windowList) {
        this.windowList = windowList;
        this.resultList = new TableModel[windowList.length];

        statPb.setParameter(OP_STATISTICS.ORDER2.pbKey(), 1);
        statPb.setParameter(OP_STATISTICS.ORDER1.pbKey(), 1);
        statPb.setParameter(OP_STATISTICS.BINARY.pbKey(), 1);
    }

    @Override
    public TableModel[] call() throws Exception {
        /*ParameterBlockImg statPb = new ParameterBlockImg(new IqmOpStatisticsDescriptor());
        statPb.setParameter(OP_STATISTICS.ORDER2.pbKey(), 1);
        statPb.setParameter(OP_STATISTICS.ORDER1.pbKey(), 1);
        statPb.setParameter(OP_STATISTICS.BINARY.pbKey(), 1);
        IqmOpStatistics opStatistics = new IqmOpStatistics();
        WorkPackage thWp = new WorkPackage(opStatistics, statPb);*/
        for (int i = 0; i < windowList.length; i++) {
            PlanarImage img = windowList[i];
            if (img != null) {
                ImageModel im = new ImageModel(img);
                //im.setModelName(im.getModelName()+"_"+i);
                //im.setProperty("disable_thumbnails", 1);
                IqmDataBox iqmDataBox = new IqmDataBox(im);
                Vector<Object> vector = new Vector<Object>();
                vector.add(iqmDataBox);
                //statPb.setSources(vector);
                thWp.updateSources(vector);
                resultList[i] = opStatistics.run(thWp).listTableResults().get(0).getTableModel();
            } else {
                System.out.println("IMG is null: " + i);
            }

        }
        return resultList;
    }
}
