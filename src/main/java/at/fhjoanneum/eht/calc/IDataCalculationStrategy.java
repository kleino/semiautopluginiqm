package at.fhjoanneum.eht.calc;

import at.mug.iqm.api.model.TableModel;
import org.apache.log4j.Logger;

import javax.media.jai.PlanarImage;
import java.util.Observer;

/**
 * Created by Kleino on 15.07.2014.
 */
public interface IDataCalculationStrategy {
    static final Logger logger = Logger.getLogger(IDataCalculationStrategy.class);

    public void addObserver(Observer o);

    public TableModel calculate(PlanarImage pi, int windowSize, int stepSize, int startValue, int endValueX, int endValueY) throws Exception;

}
