package at.fhjoanneum.eht.calc;

import at.fhjoanneum.eht.calc.multi.StatCallable;
import at.fhjoanneum.eht.utils.TableUtils;
import at.fhjoanneum.eht.utils.Utils;
import at.fhjoanneum.eht.weka.WekaClassifier;
import at.mug.iqm.api.model.TableModel;

import javax.media.jai.PlanarImage;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.*;

/**
 * Created by Kleino on 15.07.2014.
 */
public class ParallelCalculationStrategyInstantSingleWindow extends ProgressIndicator implements IDataCalculationStrategy {

    @Override
    public TableModel calculate(PlanarImage pi, int windowSize, int stepSize, int startValue, int endValueX, int endValueY) throws Exception {
        logger.info("Calculation start");
        TableModel tm = new TableModel();
        //List<Integer> indicesToRemove = Arrays.asList(0, 1, 2, 3, 4, 5, 20, 21, 22);
        //Collections.reverse(indicesToRemove);


        int scaledWidth = (int) Math.ceil(((double) endValueX - (double) startValue) / (double) stepSize);
        int scaledHeight = (int) Math.ceil(((double) endValueY - (double) startValue) / (double) stepSize);

        List<String> colNames = null;
        List<Future<TableModel>> taskResults = new ArrayList<>();

        int maxThreads = Runtime.getRuntime().availableProcessors();
        BlockingQueue<Runnable> blockingQueue = new ArrayBlockingQueue<>(maxThreads * maxThreads * maxThreads);
        RejectedExecutionHandler rejectedExecutionHandler = new ThreadPoolExecutor.CallerRunsPolicy();

        ExecutorService executor = new MonitorableThreadPool(maxThreads, maxThreads, 5, TimeUnit.MINUTES, blockingQueue, rejectedExecutionHandler, scaledWidth * scaledHeight, this);


        logger.info("Creating threads");
        for (int i = startValue; i < endValueX; i += stepSize) {
            for (int j = startValue; j < endValueY; j += stepSize) {
                int rectX = i - windowSize / 2;
                int rectY = j - windowSize / 2;
                Rectangle rect = new Rectangle(rectX, rectY, windowSize, windowSize);
                if (pi.getBounds().contains(rect)) {
                    PlanarImage window = Utils.cropImage(pi, rect);
                    taskResults.add(executor.submit(new StatCallable(window)));
                }
            }
        }

        executor.shutdown();
        logger.info("Creating result table");
        int[] indicesToRemove = new int[]{0, 1, 2, 3, 4, 5, 20, 21, 22};
        for (Future<TableModel> result : taskResults) {
            TableModel table = result.get();

            //Remove unnecessary result columns
            TableUtils.removeColums(table, indicesToRemove);

            if (colNames == null) {
                colNames = (ArrayList<String>) TableUtils.getModelColumns(table).clone();
                //More than one color band
                if (table.getRowCount() > 1) {
                    int colSize = colNames.size();
                    for (int i = 0; i < table.getRowCount() - 1; i++) {
                        for (int j = 0; j < colSize; j++) {
                            colNames.add(colNames.get(j) + "_" + (i + 1));
                        }
                    }
                }

                colNames.add("ClassAttribute");

                tm.setColumnIdentifiers(colNames.toArray());
            }

            Vector row = new Vector();
            for (Vector v : (List<Vector>) table.getDataVector()) {
                row.addAll(v);
            }
            row.add(WekaClassifier.NONE_VALUE);
            tm.addRow(row);
        }
        logger.info("Finished");
        return tm;
    }
}
