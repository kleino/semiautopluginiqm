package at.fhjoanneum.eht.calc;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.util.concurrent.ThreadPoolExecutor;

/**
 * Created by Kleino on 03.08.2014.
 */
public class MonitorThread implements Runnable {
    private final ProgressIndicator parent;
    Logger logger = LogManager.getLogger(MonitorThread.class);
    private ThreadPoolExecutor executor;

    private int numThreads;
    private int seconds;
    private boolean run = true;

    public MonitorThread(ThreadPoolExecutor executor, int numThreads, int seconds, ProgressIndicator parent) {
        this.executor = executor;
        this.numThreads = numThreads;
        this.seconds = seconds;
        this.parent = parent;
    }

    @Override
    public void run() {
        while (run) {
            float progress = (float) this.executor.getCompletedTaskCount() / (float) numThreads;

            parent.updateValue(progress);

            logger.debug(String.format("[monitor] [%d/%d] Active: %d, Completed: %d, Task: %d, isShutdown: %s, isTerminated: %s, queueSize: %s, Progress: %f",
                    this.executor.getPoolSize(),
                    this.executor.getCorePoolSize(),
                    this.executor.getActiveCount(),
                    this.executor.getCompletedTaskCount(),
                    this.executor.getTaskCount(),
                    this.executor.isShutdown(),
                    this.executor.isTerminated(),
                    this.executor.getQueue().size(),
                    progress));
            try {
                Thread.sleep(seconds * 1000);
            } catch (InterruptedException e) {
                run = false;
            }
        }

    }

}
