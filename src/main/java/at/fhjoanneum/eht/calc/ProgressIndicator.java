package at.fhjoanneum.eht.calc;

import java.util.Observable;

/**
 * Created by Kleino on 06.08.2014.
 */
public class ProgressIndicator extends Observable {

    public void updateValue(float newVal) {
        setChanged();
        notifyObservers(newVal);
    }
}
