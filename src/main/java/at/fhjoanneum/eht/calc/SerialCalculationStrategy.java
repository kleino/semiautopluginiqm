package at.fhjoanneum.eht.calc;

import at.fhjoanneum.eht.calc.multi.StatCallable;
import at.fhjoanneum.eht.params.OP_STATISTICS;
import at.fhjoanneum.eht.utils.TableUtils;
import at.fhjoanneum.eht.utils.Utils;
import at.fhjoanneum.eht.weka.WekaClassifier;
import at.mug.iqm.api.model.ImageModel;
import at.mug.iqm.api.model.IqmDataBox;
import at.mug.iqm.api.model.TableModel;
import at.mug.iqm.api.operator.ParameterBlockImg;
import at.mug.iqm.api.operator.WorkPackage;
import at.mug.iqm.img.bundle.descriptors.IqmOpStatisticsDescriptor;
import at.mug.iqm.img.bundle.op.IqmOpStatistics;

import javax.media.jai.PlanarImage;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.*;

/**
 * Created by Kleino on 10.01.2015.
 */
public class SerialCalculationStrategy extends ProgressIndicator implements IDataCalculationStrategy {
    @Override
    public TableModel calculate(PlanarImage pi, int windowSize, int stepSize, int startValue, int endValueX, int endValueY) throws Exception {
        logger.info("Calculation start");
        TableModel tm = new TableModel();

        List<String> colNames = null;
        List<TableModel> taskResults = new ArrayList<>();

        logger.info("Creating threads");
        for (int i = startValue; i < endValueX; i += stepSize) {
            for (int j = startValue; j < endValueY; j += stepSize) {
                int rectX = i - windowSize / 2;
                int rectY = j - windowSize / 2;
                Rectangle rect = new Rectangle(rectX, rectY, windowSize, windowSize);
                if (pi.getBounds().contains(rect)) {
                    PlanarImage window = Utils.cropImage(pi, rect);
                    taskResults.add(calculate(window));
                }
            }
        }

        logger.info("Creating result table");
        int[] indicesToRemove = new int[]{0, 1, 2, 3, 4, 5, 20, 21, 22};
        for (TableModel result : taskResults) {
            TableModel table = result;

            //Remove unnecessary result columns
            TableUtils.removeColums(table, indicesToRemove);

            if (colNames == null) {
                colNames = (ArrayList<String>) TableUtils.getModelColumns(table).clone();
                //More than one color band
                if (table.getRowCount() > 1) {
                    int colSize = colNames.size();
                    for (int i = 0; i < table.getRowCount() - 1; i++) {
                        for (int j = 0; j < colSize; j++) {
                            colNames.add(colNames.get(j) + "_" + (i + 1));
                        }
                    }
                }

                colNames.add("ClassAttribute");

                tm.setColumnIdentifiers(colNames.toArray());
            }

            Vector row = new Vector();
            for (Vector v : (List<Vector>) table.getDataVector()) {
                row.addAll(v);
            }
            row.add(WekaClassifier.NONE_VALUE);
            tm.addRow(row);
        }
        logger.info("Finished");
        return tm;
    }
    private TableModel calculate(PlanarImage window) {
        ParameterBlockImg statPb = new ParameterBlockImg(new IqmOpStatisticsDescriptor());
        statPb.setParameter(OP_STATISTICS.ORDER2.pbKey(), 1);
        statPb.setParameter(OP_STATISTICS.ORDER1.pbKey(), 1);
        statPb.setParameter(OP_STATISTICS.BINARY.pbKey(), 1);
        IqmOpStatistics opStatistics = new IqmOpStatistics();
        WorkPackage thWp = new WorkPackage(opStatistics, statPb);
        ImageModel im = new ImageModel(window);

        IqmDataBox iqmDataBox = new IqmDataBox(im);
        Vector<Object> vector = new Vector<Object>();
        vector.add(iqmDataBox);
        thWp.updateSources(vector);

        //TODO: Multiband?
        return opStatistics.run(thWp).listTableResults().get(0).getTableModel();
    }
}
