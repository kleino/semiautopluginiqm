package at.fhjoanneum.eht.calc;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Created by Kleino on 06.08.2014.
 */
public class MonitorableThreadPool extends ThreadPoolExecutor {
    Thread monitorThread;

    public MonitorableThreadPool(int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit, BlockingQueue<Runnable> workQueue, RejectedExecutionHandler handler, int numThreads, ProgressIndicator parent) {
        super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue, handler);
        monitorThread = createMonitorThread(this, numThreads, 3, parent);
        monitorThread.start();

    }

    private Thread createMonitorThread(MonitorableThreadPool monitorableThreadPool, int numThreads, int i, ProgressIndicator observer) {
        return new Thread(new MonitorThread(monitorableThreadPool, numThreads, 3, observer));
    }

    @Override
    protected void terminated() {
        super.terminated();
        monitorThread.interrupt();
    }
}
