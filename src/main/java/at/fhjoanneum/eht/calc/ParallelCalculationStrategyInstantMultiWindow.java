package at.fhjoanneum.eht.calc;

import at.fhjoanneum.eht.calc.multi.StatCallable2;
import at.fhjoanneum.eht.utils.TableUtils;
import at.fhjoanneum.eht.utils.Utils;
import at.fhjoanneum.eht.weka.WekaClassifier;
import at.mug.iqm.api.model.TableModel;

import javax.media.jai.PlanarImage;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.*;

/**
 * Created by Kleino on 15.07.2014.
 */
public class ParallelCalculationStrategyInstantMultiWindow extends ProgressIndicator implements IDataCalculationStrategy {

    @Override
    public TableModel calculate(PlanarImage pi, int windowSize, int stepSize, int startValue, int endValueX, int endValueY) throws Exception {
        logger.info("Calculation start");
        TableModel tm = new TableModel();

        List<String> colNames = null;

        int packageSize = 10;

        int scaledWidth = (int) Math.ceil(((double) endValueX - (double) startValue) / (double) stepSize);
        int scaledHeight = (int) Math.ceil(((double) endValueY - (double) startValue) / (double) stepSize);

        int counter = 0;

        int maxThreads = Runtime.getRuntime().availableProcessors();
        BlockingQueue<Runnable> blockingQueue = new ArrayBlockingQueue<>(maxThreads * maxThreads);
        RejectedExecutionHandler rejectedExecutionHandler = new ThreadPoolExecutor.CallerRunsPolicy();

        //ExecutorService executor = new ThreadPoolExecutor(maxThreads, maxThreads, 5, TimeUnit.MINUTES, blockingQueue, rejectedExecutionHandler);
        ExecutorService executor = new MonitorableThreadPool(maxThreads, maxThreads, 5, TimeUnit.MINUTES, blockingQueue, rejectedExecutionHandler, scaledWidth * scaledHeight, this);

        List<Future<TableModel[]>> taskResults = new ArrayList<>();
        logger.info("Creating threads");
        PlanarImage[] threadPackage = new PlanarImage[packageSize];
        for (int i = startValue; i < endValueX; i += stepSize) {
            for (int j = startValue; j < endValueY; j += stepSize) {

                int rectX = i - windowSize / 2;
                int rectY = j - windowSize / 2;
                Rectangle rect = new Rectangle(rectX, rectY, windowSize, windowSize);
                threadPackage[counter] = Utils.cropImage(pi, rect);
                if (counter == packageSize - 1) {

                    taskResults.add(executor.submit(new StatCallable2(threadPackage.clone())));
                    counter = 0;
                    threadPackage = new PlanarImage[packageSize];
                } else {
                    counter++;
                }

            }
        }
        //In case some pixels are left over, when the number of processed pixes is not evenly dividable by the packageSize
        if (threadPackage[0] != null) {
            taskResults.add(executor.submit(new StatCallable2(threadPackage)));
        }

        executor.shutdown();
        logger.info("Creating result table");
        for (Future<TableModel[]> result : taskResults) {
            TableModel[] tables = result.get();
            for (TableModel table : tables) {
                //This can happen if the number of processed pixels is not evenly dividable by the packageSize
                if (table == null) {
                    continue;
                } else {
                    if (colNames == null) {
                        colNames = (ArrayList<String>) TableUtils.getModelColumns(table).clone();
                        colNames.add("ClassAttribute");
                        tm.setColumnIdentifiers(colNames.toArray());
                    }
                    for (Vector v : (List<Vector>) table.getDataVector()) {
                        v.add(WekaClassifier.NONE_VALUE);
                        tm.addRow(v);
                    }
                }
            }
        }
        logger.info("Finished");
        return tm;
    }
}
