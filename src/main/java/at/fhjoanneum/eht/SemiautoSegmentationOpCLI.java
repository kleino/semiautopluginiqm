package at.fhjoanneum.eht;

import at.fhjoanneum.eht.gui.table.ROILayerDTO;
import at.fhjoanneum.eht.params.OP_SEMIAUTO;
import at.mug.iqm.api.gui.IDrawingLayer;
import at.mug.iqm.api.gui.roi.XMLAnnotationManager;
import at.mug.iqm.api.model.ImageModel;
import at.mug.iqm.api.model.IqmDataBox;
import at.mug.iqm.api.operator.IResult;
import at.mug.iqm.api.operator.ParameterBlockIQM;
import at.mug.iqm.api.operator.WorkPackage;
import org.apache.commons.cli.*;

import javax.imageio.ImageIO;
import javax.media.jai.PlanarImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;


/**
 * Created by Kleino on 20.08.2014.
 */
public class SemiautoSegmentationOpCLI {


    public static void main(String[] args) {
        ParameterBlockIQM pb = new ParameterBlockIQM(new SemiautoSegmentationOpDescriptor());
        SemiautoSegmentationOp iop = new SemiautoSegmentationOp();
        WorkPackage wp = new WorkPackage(iop, pb);


        Options options = new Options();

        Option roiFile = OptionBuilder.withArgName("r")
                .hasArg()
                .withDescription("path to the roi xml").isRequired()
                .create("r");
        options.addOption(roiFile);
        Option imgFile = OptionBuilder.withArgName("i")
                .hasArg()
                .withDescription("path to the image").isRequired()
                .create("i");
        options.addOption(imgFile);


        Option outputFolder = OptionBuilder.withArgName("o")
                .hasArg()
                .withDescription("output folder").isRequired()
                .create("o");
        options.addOption(outputFolder);


        //options.addOption("r", true, "");
        //options.addOption("i", true, "path to the image");
        options.addOption("s", true, "stepsize");
        options.addOption("w", true, "windowsize");

        CommandLineParser parser = new GnuParser();
        try {
            // parse the command line arguments
            CommandLine line = parser.parse(options, args);

            String roiPath = line.getOptionValue("r");
            String imgPath = line.getOptionValue("i");
            int stepSize = 5;
            int windowSize = 5;
            if (line.hasOption('s')) {
                stepSize = Integer.parseInt(line.getOptionValue('s'));
                pb.setParameter(OP_SEMIAUTO.STEP.pbKey(), stepSize);
            }
            if (line.hasOption('w')) {
                windowSize = Integer.parseInt(line.getOptionValue('w'));
                pb.setParameter(OP_SEMIAUTO.WINDOW_SIZE.pbKey(), windowSize);
            }
            String outputPath;
            if (line.hasOption('o')) {
                outputPath = line.getOptionValue('o');
            } else {
                outputPath = "D:/Programming/Masterarbeit/images/colortest" + "/" + stepSize + "_" + windowSize + "_" + System.currentTimeMillis();
            }

            //Set output images to create
            ArrayList<String> outputImages = new ArrayList<>();
            outputImages.addAll(SemiautoSegmentationOp.getAvailableImageTypes());
            outputImages.remove(3);
            outputImages.remove(2);
            pb.setParameter(OP_SEMIAUTO.RESULT_IMAGES.pbKey(), outputImages);

            //Load ROI layers
            XMLAnnotationManager mgr = new XMLAnnotationManager();
            mgr.setXmlFile(new File(roiPath));
            List<IDrawingLayer> allLayers = mgr.read();

            List<ROILayerDTO> roiLayerDTOs = new ArrayList<>();

            for (IDrawingLayer layer : allLayers) {
                roiLayerDTOs.add(new ROILayerDTO(layer.getName(), layer.getLayerColor(), "", layer.getAllROIShapes()));
            }
            pb.setParameter(OP_SEMIAUTO.LAYERS.pbKey(), roiLayerDTOs);


            //Load image
            System.setProperty("com.sun.media.jai.disableMediaLib", "true");
            PlanarImage pi = PlanarImage.wrapRenderedImage(javax.imageio.ImageIO.read(new File(imgPath)));

            ImageModel im = new ImageModel(pi);
            //im.setModelName(imgName);
            //im.setFileName(imgName + ".png");
            IqmDataBox iqmDataBox = new IqmDataBox(im);
            Vector<Object> vector = new Vector<Object>();
            vector.add(iqmDataBox);
            wp.updateSources(vector);


            IResult res = iop.run(wp);
            if (res.hasImages()) {
                //store image results
                if (outputPath != null && outputPath.isEmpty()) {
                    saveImages(res, outputPath);
                }
            }
            if (res.hasCustomResults()) {
                //Store custom results
                //Todo
            }

        } catch (ParseException exp) {
            // oops, something went wrong
            System.err.println("Parsing failed.  Reason: " + exp.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void saveImageModel(ImageModel im, String outputPath) {
        String modelName = im.getModelName().replaceAll("[\\s\\-\\+\\.\\^\\:\\,]", "");
        if (modelName.equals("")) {
            modelName = im.toString();
        }
        File output = new File(outputPath, modelName + ".png");
        output.mkdirs();
        try {
            ImageIO.write(im.getImage(), "png", output);
        } catch (IOException e) {
            System.out.println("Something went wrong when trying to save the image");
            e.printStackTrace();
        }

    }

    private static void saveImages(IResult res, String outputPath) {
        for (IqmDataBox idb : res.listImageResults()) {
            saveImageModel(idb.getImageModel(), outputPath);
        }

    }
}
