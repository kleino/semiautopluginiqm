package at.fhjoanneum.eht.weka;

import weka.classifiers.Classifier;
import weka.core.Attribute;
import weka.core.Instances;
import weka.core.SerializationHelper;
import weka.core.converters.ArffSaver;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Kleino on 7/10/14.
 */
public class WekaFileUtil {
    public static void saveAllInstances(File directory, WekaClassifier classifier) {
        HashMap<String, Instances> dataMap = classifier.getDataMap();
        for (String key : dataMap.keySet()) {
            saveInstancesToFile(directory, key, classifier);
        }
    }

    public static void saveInstancesToFile(File directory, String instanceName, WekaClassifier classifier) {

        final File dir = directory;
        final String instName = instanceName;
        final WekaClassifier cls = classifier;

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    ArffSaver saver = new ArffSaver();
                    saver.setInstances(cls.getInstancesObject(instName));
                    saver.setFile(new File(dir, instName + ".arff"));
                    saver.writeBatch();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    public static Object loadModel(File file) throws Exception {
        //Object + inputformat
        Object obj = SerializationHelper.readAll(file.getPath());

        return obj;
    }

    public static List<String> getClsClassNames(Object obj) {
        List<String> clsNames = new ArrayList<>();

        if (obj != null && obj instanceof Object[]) {
            Object[] objArr = (Object[]) obj;
            if (objArr.length > 1) {
                Instances inst = (Instances) objArr[1];

                Attribute attr = inst.classAttribute();
                for (int i = 0; i < attr.numValues(); i++) {
                    clsNames.add(attr.value(i));
                }

            }
        }

        return clsNames;
    }

    public static List<Color> getClsClassColors(Object obj) {
        //TODO: Refine me
        if (obj != null && obj instanceof Object[]) {
            Object[] objArr = (Object[]) obj;
            if (objArr.length > 2) {
                Color[] col = (Color[]) objArr[2];
                return new ArrayList<>(Arrays.asList(col));
            }
        }

        return new ArrayList<Color>();
    }

    public static void saveModel(File file, Classifier cls, Instances clsInputFormat, Color[] colors) throws Exception {
        /*
        StringBuilder classAttr = new StringBuilder();
        for (int i = 0; i < clsInputFormat.classAttribute().numValues(); i++) {
            classAttr.append(clsInputFormat.classAttribute().value(i) + ", " + colors[(i < colors.length) ? i : colors.length - 1].toString() + ";\n");
        }
        System.out.println(classAttr);     */

        SerializationHelper.writeAll(file.getPath(), new Object[]{cls, clsInputFormat, colors});
    }
}
