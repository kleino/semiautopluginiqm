package at.fhjoanneum.eht.weka;

import at.fhjoanneum.eht.utils.TableUtils;
import at.mug.iqm.api.model.TableModel;
import org.apache.log4j.Logger;
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.trees.RandomForest;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instances;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Remove;
import weka.filters.unsupervised.attribute.ReplaceMissingValues;
import weka.filters.unsupervised.attribute.StringToNominal;
import weka.filters.unsupervised.instance.RemovePercentage;
import weka.filters.unsupervised.instance.RemoveWithValues;

import java.util.*;

/**
 * Created by Kleino on 13.05.2014.
 */
public class WekaClassifier {
    public static final String TRAINING_SET = "training_set";
    public static final String TEST_SET = "test_set";
    public static final String CLASSIFICATION_SET = "classification_set";
    public static final String CLASSIFIED_SET = "classified_set";
    public static final String INITIAL_SET = "initial_set";
    public static final String NONE_VALUE = "?";
    private List<String> dummyClassNames = Arrays.asList(NONE_VALUE, "class1", "class2", "class3", "class4", "class5");
    public static final int INITIAL_CAPACITY = 200;
    private static final Logger logger = Logger.getLogger(WekaClassifier.class);
    private static final String FILTERED_SUFFIX = "_filtered";
    public static final String CLASSIFICATION_SET_FILTERED = CLASSIFICATION_SET + FILTERED_SUFFIX;
    private static final boolean USE_TEST_SET = false;
    private ArrayList<Attribute> initialAttributes;
    private HashMap<String, Instances> dataMap;
    private List<Filter> filterList;
    private Classifier cls;
    private int[] colIndicesToDrop;// = new int[]{1, 2, 3, 4, 5, 6, 21, 22, 23};

    //private int[] colIndicesToDrop = new int[]{1, 2, 3, 4, 5, 6, 7,8,9,10,11,12,13,14,15,16,17,18,19,20, 21, 22, 23};
    private Instances clsInputFormat;
    private StringBuilder evaluationResult;
    //private int[] colIndicesToDrop = new int[]{1, 2, 3, 4, 5, 6, 7};

    /**
     * Constructor for using an already trained classifier on the data
     *
     * @param tableModel the full data set
     * @param cls        the classifier
     * @throws Exception
     */
    public WekaClassifier(TableModel tableModel, Object cls) throws Exception {
        getSuppliedClassifier(cls);
        List<String> colNames = TableUtils.getModelColumns(tableModel);
        createInitialAttributes(colNames, null);
        createClassificationSet(tableModel);
        //WekaFileUtil.saveAllInstances(new File("D:\\Programming\\Masterarbeit\\images"), this);
    }

    public WekaClassifier(TableModel tableModel, Classifier untrainedClassifier, List<String> classNames) throws Exception {
        this.cls = untrainedClassifier;

        List<String> colNames = TableUtils.getModelColumns(tableModel);
        createInitialAttributes(colNames, classNames);
        createClassificationSet(tableModel);

        createTrainingSet(CLASSIFICATION_SET);

        if (USE_TEST_SET) {
            createTestSet(TRAINING_SET);
        }
        trainClassifier();
        testClassifier();
    }

    /**
     * Constructor for training a new model based on the supplied data
     *
     * @param tableModel
     * @throws Exception
     */
    public WekaClassifier(TableModel tableModel, List<String> classNames) throws Exception {
        this(tableModel, pickClassifier(), classNames);
    }

    /**
     * Pick the desired classifier
     *
     * @return
     */
    private static Classifier pickClassifier() {
        RandomForest cls = new RandomForest();
        return cls;

        /*FilteredClassifier fc = new FilteredClassifier();
        Normalize normalize = new Normalize();
        fc.setFilter(normalize);
        fc.setClassifier(new RandomForest());
        return fc;*/
    }

    /**
     * Tries to read the classifier loaded by the SerializationHelper.
     *
     * @param cls
     */
    private void getSuppliedClassifier(Object cls) {
        Object[] clsAndFormat = new Object[3];
        if (cls instanceof Object[]) {
            Object[] suppliedCLs = (Object[]) cls;
            switch (suppliedCLs.length) {
                case 1:
                    clsAndFormat[0] = suppliedCLs[0];
                    clsAndFormat[1] = null;
                    clsAndFormat[2] = null;
                    break;
                case 2:
                    clsAndFormat[0] = suppliedCLs[0];
                    clsAndFormat[1] = suppliedCLs[1];
                    clsAndFormat[2] = null;
                    break;
                case 3:
                    clsAndFormat = suppliedCLs;
                    break;
                default:
                    throw new IllegalArgumentException("Unknown classifier format");
            }
        } else if (cls instanceof Classifier) {
            clsAndFormat[0] = (Classifier) cls;
            clsAndFormat[1] = null;
            clsAndFormat[2] = null;
        } else {
            throw new IllegalArgumentException("Unknown classifier format");
        }

        if (clsAndFormat[0] == null) {
            throw new IllegalArgumentException("Unknown classifier format");
        }

        this.cls = (Classifier) clsAndFormat[0];

        Object inputFormat = clsAndFormat[1];
        if (inputFormat != null) {
            this.clsInputFormat = (Instances) inputFormat;

        } else {
            //Case 2: Classifier but no input format
            logger.info("No input format supplied for classifier, using fallback");
            this.clsInputFormat = createFallbackInputFormat(CLASSIFICATION_SET);
        }
    }

    /**
     * Create a fallback input format based on the specified set and adds some dummy class names
     *
     * @param baseFormat
     * @return
     */
    private Instances createFallbackInputFormat(String baseFormat) {
        Instances instForFormat = new Instances(getInstancesObject(baseFormat), 0);
        int classAttrIndex = instForFormat.classIndex();
        instForFormat.setClassIndex(-1);
        String oldAttrName = instForFormat.attribute(classAttrIndex).name();
        instForFormat.deleteAttributeAt(classAttrIndex);
        List<String> classNames = dummyClassNames;
        instForFormat.insertAttributeAt(new Attribute(oldAttrName, classNames), classAttrIndex);
        instForFormat.setClassIndex(classAttrIndex);
        return instForFormat;
    }

    /**
     * Create a classification by applying filters
     *
     * @param tableModel
     * @throws Exception
     */
    private void createClassificationSet(TableModel tableModel) throws Exception {
        //List<String> colNames = TableUtils.getModelColumns(tableModel);
        List<Vector> data = tableModel.getDataVector();
        dataMap = new HashMap<>();

        if (!dataMap.containsKey(CLASSIFICATION_SET)) {

            Instances newInstances = addInstances(data, CLASSIFICATION_SET);

            //Instances clsSet = applyFilter(newInstances);

            putSet(CLASSIFICATION_SET, newInstances);
            setClassAttribute(CLASSIFICATION_SET);
        }
    }

    /**
     * Remove unnecessary attributes, convert string values to nominal values and replace missing values
     *
     * @param instances
     * @return
     * @throws Exception
     */
    private Instances applyFilter(Instances instances) throws Exception {
        if (colIndicesToDrop != null && colIndicesToDrop.length > 0) {
            String[] options = new String[2];
            options[0] = "-R";                                    // "range"
            options[1] = indicesToString(colIndicesToDrop);       // first attribute
            Remove remove = new Remove();
            remove.setOptions(options);
            remove.setInputFormat(instances);
            instances = Filter.useFilter(instances, remove);
        }


        ReplaceMissingValues repl = new ReplaceMissingValues();
        repl.setInputFormat(instances);
        instances = Filter.useFilter(instances, repl);

        StringToNominal stringToNominal = new StringToNominal();
        stringToNominal.setInputFormat(instances);
        instances = Filter.useFilter(instances, stringToNominal);

        //instances.setClassIndex(instances.numAttributes());

        return instances;


    }

    /**
     * Create a training set out of the supplied setname by taking all values where the class value is not equal to the NONE_VALUE
     *
     * @param setName
     * @throws Exception
     */
    private void createTrainingSet(String setName) throws Exception {
        Instances fullSet = getInstancesObject(setName);

        int noneIndex = fullSet.classAttribute().indexOfValue(NONE_VALUE);

        String[] options = new String[2];
        options[0] = "-L";   // Nominal value index
        options[1] = String.valueOf(noneIndex + 1); //+1 because weka

        RemoveWithValues removeWithValues = new RemoveWithValues();
        Instances trainingSet = null;
        removeWithValues.setOptions(options);
        removeWithValues.setInputFormat(fullSet);
        trainingSet = Filter.useFilter(fullSet, removeWithValues);
        putSet(TRAINING_SET, trainingSet);
    }

    private void putSet(String setName, Instances instances) {
        instances.setRelationName(setName);
        dataMap.put(setName, instances);
    }

    public void createInitialAttributes(List<String> colNames, List<String> classNames) {
        if (initialAttributes == null) {
            initialAttributes = new ArrayList<Attribute>(colNames.size());
            for (String colName : colNames) {
                if (colName.equals("ClassAttribute")) {
                    initialAttributes.add(new Attribute(colName, classNames));
                } else {
                    initialAttributes.add(new Attribute(colName));
                }
            }
        }
    }

    /**
     * Sets the index of the class attribute. Will always be the last attribute of the Instances.
     *
     * @param instanceName
     */
    public void setClassAttribute(String instanceName) {
        Instances inst = getInstancesObject(instanceName);
        inst.setClassIndex(inst.numAttributes() - 1);
    }

    /**
     * Adds new instances to an instances object with the specified name. If the instances object doesn't exist yet,
     * a new instances will be created
     *
     * @param data
     * @param instanceName
     */
    private Instances addInstances(List<Vector> data, String instanceName) {
        if (!dataMap.containsKey(instanceName)) {
            System.out.println("Instances " + instanceName + " not found, creating new instances");
            Instances newInstances = new Instances(instanceName, initialAttributes, INITIAL_CAPACITY);
            putSet(instanceName, newInstances);
        }
        Instances inst = dataMap.get(instanceName);
        for (Vector<Double> row : data) {
            addInstance(inst, row);
        }
        return inst;
    }

    /**
     * Add one instance to an instances object
     *
     * @param inst
     * @param row
     * @return
     */
    private boolean addInstance(Instances inst, Vector<Double> row) {
        double[] vals = new double[inst.numAttributes()];

        for (int i = 0; i < inst.numAttributes(); i++) {
            Object obj = row.get(i);
            if (obj instanceof Double) {
                vals[i] = (double) obj;
            } else if (obj instanceof Integer) {
                vals[i] = (int) obj;
            } else if (obj instanceof Float) {
                vals[i] = (float) obj;
            } else if (obj instanceof String) {

                if (inst.attribute(i).isNominal()) {
                    //TODO: What attribute does not contain value
                    vals[i] = inst.attribute(i).indexOfValue(String.valueOf(obj));
                } else {
                    if (!inst.attribute(i).isString()) {
                        for (int j = 0; j < inst.attribute(i).numValues(); j++) {
                            System.out.println(inst.attribute(i).value(j) + "----");
                        }
                        String oldAttrName = inst.attribute(i).name();
                        inst.deleteAttributeAt(i);
                        inst.insertAttributeAt(new Attribute(oldAttrName, (ArrayList<String>) null), i);
                    }
                    vals[i] = inst.attribute(i).addStringValue(String.valueOf(obj));
                }

            } else {
                //Value is of other type, nothing added to vals
            }
        }
        return inst.add(new DenseInstance(1.0, vals));
    }

    /**
     * Train the classifier using the TRAINING_SET
     *
     * @throws Exception
     */
    public void trainClassifier() throws Exception {
        Instances dataForTraining = getInstancesObject(TRAINING_SET);

        //TODO: Class attribute is set here?
        clsInputFormat = new Instances(dataForTraining, 0);

        //cls = pickClassifier();
        cls.buildClassifier(dataForTraining);


    }

    public Instances getInstancesObject(String instancesName) {
        Instances inst = dataMap.get(instancesName);
        if (inst == null) {
            logger.error("Error: Instance " + instancesName + " does not exist");
            return null;
        } else {
            return inst;
        }
    }

    public void testClassifier() throws Exception {
        Instances inst = null;
        if (USE_TEST_SET) {
            inst = getInstancesObject(TEST_SET);
        } else {
            inst = getInstancesObject(TRAINING_SET);
        }

        String summary = "";
        String classDetail = "";
        String confMatrix = "";
        String evaluationType;
        Evaluation eval = new Evaluation(inst);
        if (USE_TEST_SET) {
            logger.info("Using testset");
            evaluationType = "testset";
            eval.evaluateModel(cls, inst);
        } else {
            evaluationType = "crossvalidation";
            logger.info("Using crossvalidation");
            Random rand = new Random(1);
            eval.crossValidateModel(cls, inst, 10, rand, new Object[]{});
        }


        summary = eval.toSummaryString();
        classDetail = eval.toClassDetailsString();
        confMatrix = eval.toMatrixString();
        logger.info("Evaluation: " + summary);

        evaluationResult = new StringBuilder();
        evaluationResult.append("--Evaluation result:\n");
        evaluationResult.append(new Date());
        evaluationResult.append('\n');
        evaluationResult.append("Classifier: ");
        evaluationResult.append(cls.toString().trim());
        evaluationResult.append('\n');
        evaluationResult.append('\n');

        evaluationResult.append("Evaluation type: ");
        evaluationResult.append(evaluationType);
        evaluationResult.append('\n');

        evaluationResult.append(summary);
        evaluationResult.append(classDetail);
        evaluationResult.append(confMatrix);
        evaluationResult.append('\n');

    }

    public void useClassifier() throws Exception {
        Instances unlabeled = getInstancesObject(CLASSIFICATION_SET);

        //Make sure that the classification set has the correct number of class attributes. This will happen if a
        // classifier is loaded because the inital data set does not contain any class values except for the NONE_VALUE
        if (unlabeled.classAttribute().numValues() != clsInputFormat.classAttribute().numValues()) {
            Instances newInst = new Instances(clsInputFormat);
            newInst.addAll(unlabeled);
            unlabeled = newInst;
        }

        Instances labeled = new Instances(unlabeled);
        for (int i = 0; i < unlabeled.numInstances(); i++) {
            double label = cls.classifyInstance(unlabeled.instance(i));
            labeled.instance(i).setClassValue(label);
        }
        putSet(CLASSIFIED_SET, labeled);
    }

    private String indicesToString(int[] indices) {
        StringBuilder sb = new StringBuilder();
        char delimiter = ',';
        for (int i = 0; i < indices.length; i++) {
            sb.append(indices[i]);
            sb.append(delimiter);
        }
        return sb.toString();
    }

    public void createTestSet(String trainingSetName) throws Exception {
        Instances trainingSet = getInstancesObject(trainingSetName);

        Instances shuffledSet = new Instances(trainingSet);
        shuffledSet.randomize(new Random());

        Instances testSet = null;

        RemovePercentage removePercentage = new RemovePercentage();
        String[] options = new String[4];
        options[0] = "-P";
        options[1] = "10";
        options[2] = "";
        options[3] = "";

        removePercentage.setOptions(options);
        removePercentage.setInputFormat(shuffledSet);
        trainingSet = Filter.useFilter(shuffledSet, removePercentage);

        options = new String[3];

        options[0] = "-P";
        options[1] = "10";
        options[2] = "-V";

        removePercentage.setOptions(options);
        testSet = Filter.useFilter(shuffledSet, removePercentage);

        putSet(TEST_SET, testSet);
        putSet(TRAINING_SET, trainingSet);
    }

    public Classifier getCls() {
        return cls;
    }

    public HashMap<String, Instances> getDataMap() {
        return dataMap;
    }

    public List<String> getDummyClassNames() {
        return dummyClassNames;
    }

    public void setColIndicesToDrop(int[] colIndicesToDrop) {
        this.colIndicesToDrop = colIndicesToDrop;
    }

    public String getEvaluationResult() {
        if (evaluationResult != null) {
            return evaluationResult.toString();
        } else {
            return "";
        }
    }

    public Instances getClsInputFormat() {
        return clsInputFormat;
    }
}
